package arrayQ;


public class MaxSumSubarray {

    // Question : Find the max sum subarray of a fixed size K

    public static void main(String[] args) {
        System.out.println(MaxSumSubarray.findMaxSumSubarray(new int[]{4, 2, 1, 7, 8, 1, 2, 8, 1, 0}, 3));
    }


    public static int findMaxSumSubarray(int[] arr, int K) {
        int maxValue = Integer.MIN_VALUE;
        int currentRunningSum = 0;

        // Iterate over the entire array
        for (int i = 0; i < arr.length; i++) {

            // Add our value to current running sum
            // For the next window, it will add incoming value
            currentRunningSum = currentRunningSum + arr[i];

            // Question : Find difference
            //     currentRunningSum += arr[i];

            // Caveat : Is the current running sum greater than the max value that we have recorded
            // Is the value of i is greater than or equal to K-1 because value of i says how far we are in array
            // and as i is the index of array, effective value of K would be K-1, i.e at i = K - 1, the window has K elements

            if (i >= K - 1) {

                // Now we have K elements in our list. Compare the current running sum with max value recorded

                maxValue = Math.max(maxValue, currentRunningSum);
                // For the next window, remove the first element from the current window - done below
                // and add the next element  - done above
                // Note that it being an array, effective K is K -1

                currentRunningSum = currentRunningSum - arr[i - (K - 1)];
            }
        }
        return maxValue;
    }
}
