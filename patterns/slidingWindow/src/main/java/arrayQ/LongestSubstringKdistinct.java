// TODO : LC - 340

package arrayQ;

import java.util.HashMap;
import java.util.Map;

public class LongestSubstringKdistinct {

    // Note : A dynamic variant of sliding window, with ADT

    // Question : Longest substring length with K distinct characters

    public static void main(String[] args) {

        String str = "AAAHHIBC";
        int size = 2;
        System.out.println(findLength(str, size));
    }


    private static int findLength(String str, int K) {
        Map<Character, Integer> charFrequencyMap = new HashMap<>();
        int maxLength = 0;
        int windowStart = 0;


        for (int windowEnd = 0; windowEnd < str.length(); windowEnd++) {
            char rightChar = str.charAt(windowEnd);
            charFrequencyMap.put(rightChar, charFrequencyMap.getOrDefault(rightChar, 0) + 1);

            while (charFrequencyMap.size() > K) {
                char leftChar = str.charAt(windowStart);
                charFrequencyMap.put(leftChar, charFrequencyMap.getOrDefault(leftChar, 0) - 1);
                if (charFrequencyMap.get(leftChar) == 0) {
                    charFrequencyMap.remove(leftChar);
                }
                windowStart++;
            }
            maxLength = Math.max(maxLength, windowEnd - windowStart + 1);
        }
        return maxLength;
    }

    public int lengthOfLongestSubstringKDistinct(String s, int k) {
        int result = 0;
        int i=0;
        HashMap<Character, Integer> map = new HashMap<Character, Integer>();

        for(int j=0; j<s.length(); j++){
            char c = s.charAt(j);
            if(map.containsKey(c)){
                map.put(c, map.get(c)+1);
            }else{
                map.put(c, 1);
            }

            if(map.size()<=k){
                result = Math.max(result, j-i+1);
            }else{
                while(map.size()>k){
                    char l = s.charAt(i);
                    int count = map.get(l);
                    if(count==1){
                        map.remove(l);
                    }else{
                        map.put(l, map.get(l)-1);
                    }
                    i++;
                }
            }

        }

        return result;
    }
}


