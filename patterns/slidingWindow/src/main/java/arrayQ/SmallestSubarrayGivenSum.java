package arrayQ;

public class SmallestSubarrayGivenSum {

    // Question : Find the size of smallest subarray with a given sum
    // samllest sum >= given sum
    // Caveat : Window size is not fix, we are iterating both side of window
        // Use two pointers for window

    public static void main(String[] args) {
        int [] input = new int[]{4,2,2,7,8,1,2,8,10};
        int targetSum = 8;
        System.out.println(smallestSubarray(targetSum,input));
    }

    private static int smallestSubarray(int targetSum, int[] arr) {

        int windowStart = 0;
        int currentWindowSum = 0;
        int minWindowSize = Integer.MAX_VALUE;

        // Window end always iteration towards end

        for(int windowEnd = 0; windowEnd < arr.length; windowEnd++ )
        {
            // Grow the right side
            currentWindowSum += arr[windowEnd];

            // Keep on adding value till we reach the constraint runningSum >= targetSum
            // And then start optimizing the window
            // use while, as we need to shrink left hand side

            while(currentWindowSum>=targetSum)
            {
                // Record the best we have
                // Caveat : Add 1 as the difference is inclusive i.e 1,2,3 has size of 3 and not (3-1)

                minWindowSize = Math.min(minWindowSize, windowEnd-windowStart + 1);

                // Remove far left value
                currentWindowSum -= arr[windowStart];

                // Shrinking left hand side
                windowStart++;
            }
        }
        return minWindowSize;
    }
}
