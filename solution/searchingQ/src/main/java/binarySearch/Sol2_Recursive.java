package binarySearch;

public class Sol2_Recursive implements ISolution {


    public static int binarySearchRecursive(int element, int[] arr, int low, int high) {
        // Note : Handle the negative case, element not present in array
        if (high > low) {
            // Note : Only need to evaluate mid in positive scenarios
            int mid = low + (high - low) / 2; // 0 + 3
            if (arr[mid] > element) {
                return binarySearchRecursive(element, arr, low, mid - 1);
            } else if (arr[mid] < element) {
                return binarySearchRecursive(element, arr, mid + 1, high);
            }
            return mid;
        }
        return -1;
    }
}
