package binarySearch;

public class Sol1_Iterative  implements ISolution {


    public static int binarySearchIterative(int element, int[] arr) {
        int low = 0;
        int high = arr.length - 1;
        int mid;

        // Note : Better to use pointer that a for loop to half.. Also pay attention to less or equal to
        while (low <= high) {
            mid = low + (high - low) / 2;
            if (arr[mid] > element) {
                high = mid - 1;
            } else if (arr[mid] < element) {
                low = mid + 1;
            } else {
                return mid;
            }
        }
        return -1;
    }
}
