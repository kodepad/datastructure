package binarySearch;

public class SolutionDriver {
    static int[] arr = {2, 4, 5, 6, 7, 8, 9};
    static int elementToSearch = 8;


    public static void main(String[] args) {


        System.out.println("Iterative Search");
        int index = Sol1_Iterative.binarySearchIterative(elementToSearch, arr);
        System.out.println(index);

        System.out.println("Recursive Search");
        int index2 = Sol2_Recursive.binarySearchRecursive(elementToSearch, arr, 0, arr.length - 1);
        System.out.println(index2);
    }
}
