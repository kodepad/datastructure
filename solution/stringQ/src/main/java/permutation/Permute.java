package permutation;

public class Permute {

    public static void main(String[] args) {
        permuateAndPrint("SAME");

    }

    public static void permuateAndPrint(String str) {
        permuateAndPrint("", str);
    }

    public static void permuateAndPrint(String prefix, String str)
    {
        int n = str.length();
        if(n==0)
        {
            System.out.println(prefix + "");
        }
        else{
            for(int i =0; i < str.length(); i++)
            {
                permuateAndPrint(prefix + str.charAt(i), str.substring(i+1,n) + str.substring(0,i));
            }
        }
    }
}
