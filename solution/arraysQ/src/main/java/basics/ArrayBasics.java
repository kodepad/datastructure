package basics;

import java.util.Arrays;

public class ArrayBasics {


    public static void main(String[] args) {
        Integer [] arr = {1,2,3,4,5};
        Integer [] inputArray = new Integer[10];
        Integer [] outputArray;
        System.arraycopy(arr,0, inputArray, 0, arr.length);

        printArray(inputArray);
        System.out.println("Inserting 20 at index 2");
        outputArray = insertElementAtIndex(20,2, inputArray);

        System.out.println("===Output Array===");
        printArray(outputArray);
        System.out.println("===Input Array===");
        printArray(inputArray);


        //Caveat : Both input and output array are same because ARRAYS ARE PASS BY REFERENCE
        if (Arrays.equals(inputArray,outputArray))
            System.out.println("Both arrays are equal");
        else
            System.out.println("Arrays are not equal");

        System.out.println("Deleting 3rd index element");
        outputArray = deleteSingleElementAtIndex(3, inputArray);
        printArray(outputArray);
    }

    private static void printArray(Integer[] array)
    {
        System.out.println(Arrays.toString(array));
    }

    private static Integer[] insertElementAtIndex(int element, int index, Integer[] array)
    {
/*         Iterate to last element,
         Note : swap positions from the end of array
         and insert
         Note : Do not call size() in for loop, as it would keep on increasing itself, ARRAY IS PASS BY REFERENCE
         */
        int currentSizeOfArray = findSizeOfArray(array);
        for(int i = currentSizeOfArray; i >= index; i--)
        {
            array[i+1] = array[i];
        }
        array[index] = element;
        return array;
    }

    private static int findSizeOfArray(Integer[] arr)
    {
        int index;
        for( index = 0; index < arr.length; index ++)
        {
            if(arr[index]==null)
            {
                break;
            }
        }
        return index;
    }

    private static Integer[] deleteSingleElementAtIndex(int index, Integer[] arr){
        int sizeOfArray = findSizeOfArray(arr);
        for (int i = index; i < sizeOfArray ; i++) {
            arr[i] = arr[i + 1];
        }
        return arr;
    }

}
