package twoSum;

public interface Solutions {

    // Note 1 : Static methods in interface should have a body

    int[] twoNumberSum(int[] array, int targetSum);

}
