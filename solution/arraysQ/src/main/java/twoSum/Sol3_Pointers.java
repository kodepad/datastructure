// AE
package twoSum;

import java.util.Arrays;

public class Sol3_Pointers implements Solutions {

    // Note 1 : while condition is put on pointers and not on the sum
    // Question : Should i put it on sum or pointers ?

    // jhg kjh

    @Override
    public int[] twoNumberSum(int[] array, int targetSum) {
        Arrays.sort(array);
        int left = 0;
        int right = array.length - 1;
        while (left < right) {
            int currentSum = array[left] + array[right];
            if (currentSum == targetSum) {
                return new int[] {array[left], array[right]};
            } else if (currentSum < targetSum) {
                left++;
            } else if (currentSum > targetSum) {
                right--;
            }
        }
        return new int[0];
    }
}