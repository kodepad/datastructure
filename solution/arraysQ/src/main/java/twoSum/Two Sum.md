https://www.jetbrains.com/help/idea/todo-example.html 

#### Question
####Two number sum

Write a function that takes in a non-empty array of **distinct** integers and an integer representing a target sum. 

If any two numbers in the input array sum up to the target sum, the function should return them in an array. 
If no two numbers sum up to the target sum, the function should return an empty array. 

Assume that there will be **at most one pair** of numbers summing up to the target sum.

#### Notes
* Array of distinct numbers, hence no duplicate number
* Returns - At most one pair 

#### Caveats
* Make sure their solution correctly handles the case where there are duplicated elements in the array
* Don't accidentally let you pair an element with itself

#### Solutions

| Approach    | time complexity | space complexity |
|-------------|-----------------|------------------|
| Brute Force | O(n^2)          | O(1)             |
| Hashing     | O(n)            | O(n)             |
| Pointers    | O(nlogn)        | O(n)             |

#### Resources
* https://web.stanford.edu/class/cs9/sample_probs/TwoSum.pdf
* https://www.geeksforgeeks.org/given-an-array-a-and-a-number-x-check-for-pair-in-a-with-sum-as-x/

##### Further Questions
* How to handle duplicate elements in the array.