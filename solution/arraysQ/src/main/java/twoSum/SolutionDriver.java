package twoSum;


public class SolutionDriver {

    public static int[] input1 = {3, 5, -4, 8, 11, 1, -1, 6};
    private static int[] input2 = {3, 5, 8, 11, 1, 6};
    private static int targetSum = 10;

    public static void main(String[] args) {

        Solutions[] solutions = new Solutions[]{new Sol1_BruteForce(), new Sol2_Hashing(), new Sol3_Pointers()};

        for (Solutions solution : solutions) {
            System.out.println(
                    "Result from ".concat(String.valueOf(solution.getClass().getSimpleName())));

            int[] result = solution.twoNumberSum(input1, targetSum);
            for (int i : result) {
                System.out.println(i);
            }
        }
    }
}