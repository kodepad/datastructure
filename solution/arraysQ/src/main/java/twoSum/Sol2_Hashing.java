// AE
package twoSum;

import java.util.HashMap;
import java.util.Map;

public class Sol2_Hashing implements Solutions {
    Map<Integer, Boolean> map = new HashMap();

/*   NOTE : Hashing -   O(n) time | O(n) space

     Caveat 1 : Make sure their solution correctly handles the case where there are duplicated elements in the array
     Caveat 2 : Don't accidentally let you pair an element with itself.

     Question 1 : If i have duplicate value here, won't it again fall in the second scenario
     Question 2 : How to handle the scenario of an element equal to target / 2
  */

    @Override
    public int[] twoNumberSum(int[] array, int targetSum) {
        Map<Integer, Boolean> nums = new HashMap<>();
        for (int num : array) {
            int potentialMatch = targetSum - num;
            if (nums.containsKey(potentialMatch)) {
                return new int[]{potentialMatch, num};
            } else {
                nums.put(num, true);
            }
        }
        return new int[0];
    }
}