package twoSumVariant;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Solution {


    public static void main(String[] args) {
        int[] input = {2, 7, 11, 15};
        int targetSum = 9;
        int[] output = twoSum(input, targetSum);
        System.out.println(Arrays.toString(output));
    }


        public static int[] twoSum(int[] nums, int target) {
            Map<Integer,Integer> map = new HashMap<>();

            for(int i = 0; i < nums.length; i++)
            {
                int diff = target-nums[i];
                if(map.containsKey(diff))
                {
                    return new int[]{i, map.get(diff)};
                }
                map.put(nums[i],i);
            }
            return new int[]{0};
        }

}
