package ValidatieSubsequence;

import java.util.List;

public interface Solutions {

    public boolean isValidSubsequence(List<Integer> array, List<Integer> sequence);
}
