package ValidatieSubsequence;

import java.util.Arrays;
import java.util.List;

public class SolutionDriver {

    public static List<Integer> arrayList = Arrays.asList(new Integer [] {3, 5, -4, 8, 11, 1, -1, 6});
    private static List<Integer> sequenceList = Arrays.asList(new Integer [] {3, 5, 8, 11, 1, 6});

    public static void main(String[] args) {

        Solutions[] solutions = new Solutions[]{new Sol1_forLoop(), new Sol2_whileLoop()};

        for (Solutions solution : solutions) {
            System.out.println(
                    "Result from ".concat(String.valueOf(solution.getClass().getSimpleName())));

            boolean result = solution.isValidSubsequence(arrayList, sequenceList);
            System.out.println(result);
        }
    }
}
