package ValidatieSubsequence;

import java.util.List;

public class Sol1_forLoop implements Solutions {

    @Override
    public  boolean isValidSubsequence(List<Integer> array, List<Integer> sequence) {
        int seqIdx = 0;
        for (int value : array) {
            if (seqIdx == sequence.size()) {
                break;
            }
            if (sequence.get(seqIdx).equals(value)) {
                seqIdx++;
            }
        }
        return seqIdx == sequence.size();
    }
}
