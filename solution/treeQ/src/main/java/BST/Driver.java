package BST;


import java.util.List;

public class Driver {


    public static void main(String[] args)
    {
        // BST
        BinarySearchTree BSTtree1 = new BinarySearchTree();
        // Level 0
        BSTtree1.root = new Node("25");

        // Level 1
        BSTtree1.root.left = new Node("15");
        BSTtree1.root.right = new Node("50");

        // Level 2
        BSTtree1.root.left.left = new Node("10");
        BSTtree1.root.left.right = new Node("22");
        BSTtree1.root.right.left = new Node("35");
        BSTtree1.root.right.right = new Node("70");

        // Level 3
        BSTtree1.root.left.left.left = new Node("4");
        BSTtree1.root.left.left.right = new Node("12");
        BSTtree1.root.left.right.left = new Node("18");
        BSTtree1.root.left.right.right = new Node("24");
        BSTtree1.root.right.left.left = new Node("31");
        BSTtree1.root.right.left.right = new Node("44");
        BSTtree1.root.right.right.left = new Node("66");
        BSTtree1.root.right.right.right = new Node("90");


        // Note : Traversal
        System.out.println("============");
        TreePrinter.print(BSTtree1.root);
        System.out.println("Preorder traversal of binary tree is ");
        BSTtree1.preOrderRecursive(BSTtree1.root);

        System.out.println("\nInorder traversal of binary tree is ");
        BSTtree1.inOrderRecursive(BSTtree1.root);

        System.out.println("\nPostorder traversal of binary tree is ");
        BSTtree1.postOrderRecursive(BSTtree1.root);


        // Note : Running Sum - Leetcode 938
        int lowerBound = 15;
        int upperBound = 30;
        System.out.format(" \n Running sum of BST for lowerBound %d and upperBound %d is ", lowerBound, upperBound);
        System.out.println(BSTtree1.runningSumInclusive(BSTtree1.root,lowerBound,upperBound));

        System.out.println("======");
        System.out.println("BFS of tree");
        BSTtree1.BFS(BSTtree1.root);

        System.out.println("BFS 2");
        BSTtree1.BFS(BSTtree1.root);

    }
}

