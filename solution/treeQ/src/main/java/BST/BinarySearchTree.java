package BST;


import java.util.LinkedList;
import java.util.Queue;

public class BinarySearchTree {
    public Node root;

    public BinarySearchTree() {
        root = null;
    }


    /*
        Traversals
        Note : The sequence of child would always be LEFT and RIGHT
        CAVEAT : Always check for not null, else NPE
        Using visit method to carry out different tasks such as storing in array
     */

    public void preOrderRecursive(Node node) {
        if (node != null) {
            visit(node);
            preOrderRecursive(node.left);
            preOrderRecursive(node.right);
        }
    }

    public void inOrderRecursive(Node node) {
        if (node != null) {
            inOrderRecursive(node.left);
            visit(node);
            inOrderRecursive(node.right);
        }
    }

    public void postOrderRecursive(Node node) {
        if (node != null) {
            postOrderRecursive(node.left);
            postOrderRecursive(node.right);
            visit(node);
        }
    }

    // Question : Giving same result as PreOrder Traversal
    public void DFS(Node node) {
        Queue<Node> queue = new LinkedList();

        queue.add(node);
        while(!queue.isEmpty())
        {
            Node currentNode = queue.remove();
            System.out.println(currentNode.key);

            if(currentNode.left!=null){
                DFS(currentNode.left);
            }
            if (currentNode.right!=null){
                DFS(currentNode.right);
            }
        }
    }


    // Note : It is not a recursive call
    // If i dont remove from q at the startup and wait until the end, it would end up in infinite loop
    // Check for empty and remove keep it exit at correct time.
    public void BFS(Node node)
    {
        Queue<Node> queue = new LinkedList();

        queue.add(node);
        while(!queue.isEmpty())
        {
            Node currentNode = queue.remove();
            System.out.println(currentNode.key);

            if(currentNode.left != null)
            {
                queue.add(currentNode.left);
            }
            if(currentNode.right != null)
            {
                queue.add(currentNode.right);
            }
        }
    }


    // https://www.youtube.com/watch?v=uWL6FJhq5fM
    public int runningSumInclusive(Node root, int lowerBound, int upperBound) {
        int sum = 0;
        int i = 0;
        if (root == null)
            return sum;

        Queue<Node> queue = new LinkedList<>();
        queue.add(root);

        while (!queue.isEmpty()) {
            // Some sort of processing to be done when there are elements in queue

            // Get the node in queue
            Node currentNode = queue.remove();

            // If current value is between range, add it in the sum
            if (Integer.parseInt(currentNode.key) >= lowerBound && Integer.parseInt(currentNode.key) <= upperBound) {
                i = Integer.parseInt(currentNode.key);
                sum = sum + i;
            }

            /*
             Do we have left node.
                - If yes, should I go there ?
            No, if parent node value is lower than my lower bound
                As all left children will have lower values and not needed for running sum
             i.e I will traver left BST only if the parent node has value greater than lowerBound

            */

            if (currentNode.left != null && Integer.parseInt(currentNode.key) > lowerBound) {
                queue.add(currentNode.left);
            }

            /*
             Do we have right node.
                - If yes, should I go there ?
            No, if parent node value is greater than my upper bound
                As all left children will have higher values and not needed for running sum
             i.e I will traver right BST only if the parent node has value lower than upperBound
            */
            if (currentNode.right != null && Integer.parseInt(currentNode.key) < upperBound) {
                queue.add(currentNode.right);
            }
        }
        return sum;
    }


    private void visit(Node node) {
        System.out.println(node.key);
    }
}

// Note : If wrapping Node inside Binary tree, use STATIC CLASS
// For the purposes of interview questions, we typically do not use a Tree class

class Node implements TreePrinter.PrintableNode {
    String key;
    Node left;
    Node right;

    public Node(String key) {
        this.key = key;
        this.left = this.right = null;
    }

    @Override
    public TreePrinter.PrintableNode getLeft() {
        return left;
    }

    @Override
    public TreePrinter.PrintableNode getRight() {
        return right;
    }

    @Override
    public String getText() {
        return key;
    }


}