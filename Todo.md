
* Integer to int
* Integer to String
* String to Integer

* Modules to add  


     <modules>
         <module>arraysQ</module>
         <module>stringQ</module>
         <module>listQ</module>
         <module>hashTables</module>
         <module>treeQ</module>
         <module>graphQ</module>
         <module>recursion</module>
         <module>backtrackingQ</module>
         <module>searchingQ</module>
         <module>sortingQ</module>
         <module>dynamicProgrammingQ</module>
         <module>coreJavaPoints</module>
     </modules>

 </project>`