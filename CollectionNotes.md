Arrays.asList(array)


List.toArray(list)


Collections.reverse(collection)

map.get(key)  - null when key is not present
map.getOrDefault(key, defalutValue)
map.size()

Map.get returns if key is not present. If uncertain, use map.getOrDefault  
The java.util.HashMap.size() method of HashMap class is used to get the size of the map which refers to the number of the key-value pair or mappings in the Map.