https://javarevisited.blogspot.com/2015/01/top-20-string-coding-interview-question-programming-interview.html

### Strings
1. How to Print duplicate characters from String
2. How to check if two Strings are anagrams of each other
3. How to program to print first non repeated character from String
4. How to reverse String in Java using Iteration and Recursion
5. How to check if a String contains only digits
6. How to find duplicate characters in a String
7. How to count a number of vowels and consonants in a String
8. How to count the occurrence of a given character in String
9. How to convert numeric String to an int
10. How to replace each given character to other e.g. blank with %20
11. How to find all permutations of String
12. How to reverse words in a sentence without using a library method
13. How to check if String is Palindrome
14. How to remove duplicate characters from String
15. How to check if a String is a valid shuffle of two String
16. Write a program to check if a String contains another String like indexOf ()
17. How to return the highest occurred character in a String
18. Write a program to remove a given character from String
19. Write a program to find the longest palindrome in a string
20. How to sort String on their length in Java
21. How to find the length of the longest substring with K distinct characters