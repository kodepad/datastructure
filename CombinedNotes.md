##### Setting custom pattern such  as TODO in  Idea
https://www.jetbrains.com/help/idea/using-todo.html

Removing duplicate code inspection  
https://stackoverflow.com/questions/36627002/is-it-possible-to-disable-duplicate-code-detection-in-intellij
#1.basicP

* Static methods in interface should have a body
    * static int[] twoNumberSum( int[] array, int targetSum);

* An abstract method is defined only so that it can be overridden in a subclass.
    * However, static methods can not be overridden.
    * Therefore, it is a compile-time error to have an abstract, static method.
    * It's because static methods belongs to a particular class and not to its instance.

* Links
    * https://stackoverflow.com/questions/370962/why-cant-static-methods-be-abstract-in-java
   
####Question
Check fraction and Math Class in Java

* https://www.guru99.com/math-java.html
---
#2.Array

#2.1basic

#### Array Initialization

* https://www.geeksforgeeks.org/arrays-in-java/
* https://www.educative.io/edpresso/how-to-initialize-an-array-in-java
* https://www.journaldev.com/750/initialize-array-java#initializing-an-array-in-java-8211-primitive-type
* https://www.geeksforgeeks.org/default-array-values-in-java/
* https://www.baeldung.com/java-initialize-array


You can only use the abbreviated syntax `int[] pos = {0,1,2};` at variable initialization time.
At other places, we should use `pos = new int[]{1,2,3};`

* https://stackoverflow.com/questions/41658497/array-initializer-is-not-allowed-here

To print the element of arrays, use  
`Arrays.toString(arr);`

Always for an index equality, use   
`MaxIndex = arr.length - 1`


For primitive types, the array will be initialized with the default values (0 for int).  
You can make it `Integer [] theArray = new Integer[20]`, it will have null for unassigned values

ArrayList has a size(), Arrays / arrays do not have one [1]

`System.arraycopy` can be used to copy some elements from one array to another [2]

Always use `Integer[]` in coding 
int[] vs Integer[]  [2.1]


Array vs Arrays [3][3.1]

Arrays pass by reference or pass by value [4] [5]

[1] https://stackoverflow.com/questions/4441099/how-do-you-count-the-elements-of-an-array-in-java  
[2] https://docs.oracle.com/javase/7/docs/api/java/lang/System.html#arraycopy  
[2.1] https://www.techiedelight.com/convert-int-array-integer-array-java/  
[2.2] https://stackoverflow.com/questions/3571352/how-to-convert-integer-to-int   
[2.3] https://stackoverflow.com/questions/13747859/how-to-check-if-an-int-is-a-null  
[2.4] https://stackoverflow.com/questions/45278904/checking-integer-wrapper-against-null-as-well-as-primitive-value-0  
[2.5] https://stackoverflow.com/questions/41183668/how-to-check-whether-an-integer-is-null-or-zero-in-java

[3] https://stackoverflow.com/questions/35760899/why-we-have-arrays-and-array-in-java  
[3.1] http://tutorials.jenkov.com/java/arrays.html#inserting-elements-into-an-array  
[4] https://stackoverflow.com/questions/12757841/are-arrays-passed-by-value-or-passed-by-reference-in-java  
[5] https://stackoverflow.com/questions/21653048/changing-array-in-method-changes-array-outside  
[6] https://stackoverflow.com/questions/40480/is-java-pass-by-reference-or-pass-by-value


#### InPlace and OutPlace

##### InPlace
 An in-place method modifies data structures or objects outside of its own stack frame (i.e.: stored on the process heap or in the stack frame of a calling function). Because of this, the changes made by the method remain after the call completes.
    
##### OutPlace
 An out-of-place method doesn't make any changes that are visible to other methods. Usually, those methods copy any data structures or objects before manipulating and changing them.    
 
 
Working in-place is a good way to save time and space. An in-place algorithm avoids the cost of initializing or copying data structures, and it usually has an O(1)O(1) space cost.

But be careful: an in-place algorithm can cause side effects. Your input is "destroyed" or "altered," which can affect code outside of your method. For example:


**Generally, out-of-place algorithms are considered safer because they avoid side effects.** You should only use an in-place algorithm if you're space constrained or you're positive you don't need the original input anymore, even for debugging.

---

##2.2TwoSum

##### Array

`An object is a class instance or an array.
`

`The reference values (often just references) are pointers to these objects, and a special null reference, which refers to no object.
`

`Arrays are objects, are dynamically created, and may be assigned to variables of type Object, All methods of class Object may be invoked on an array.
`

`Every array has an associated Class object, shared with all other arrays with the same component type.
`


* An array is an object in Java
* Every Type in java has an implicit class for the array of Type
* These classes are not accessible for end users
* Object class is the supertype of an array

####Useful Links
* https://docs.oracle.com/javase/specs/jls/se7/html/jls-10.html
* https://docs.oracle.com/javase/specs/jls/se7/html/jls-4.html#jls-4.3.1
* https://docs.oracle.com/javase/specs/jls/se7/html/jls-10.html#jls-10.8

##### Map.get(key)
* Returns the value to which the specified key is mapped, or null if this map contains no mapping for the key.
* NullPointerException if the specified key is null and this map does not permit null keys
        

##### Map.containsKey(key) 

* Will return true or false based on whether key is present or not
* If the specified key is null and this map does not permit null keys 

##### Return ternary operation expression

Java ternary operator is an expression, and is thus evaluated to a single value

`return expression-1 ? expression-2 : expression-3`

where 
* expression-1 has type boolean and 
* expression-2 and expression-3 have the same type.
* https://stackoverflow.com/a/25072826/6465925

##### final

* The way the final keyword works in Java is that the variable's pointer to the value cannot change. Let's repeat that: it's the pointer that cannot change the location to which it's pointing.
    * There's no guarantee that the object being referenced will stay the same
    * only that the variable will always hold a reference to the same object. 
    * If the referenced object is mutable (i.e. has fields that can be changed), then the constant variable may contain a value other than what was originally assigned. 
    * https://www.thoughtco.com/constant-2034049

* If a final variable holds a reference to an object, then the state of the object may be changed by operations on the object, but the variable will always refer to the same object.  
https://docs.oracle.com/javase/specs/jls/se7/html/jls-4.html#jls-4.12.4

##### constant

As per java specs (https://docs.oracle.com/javase/specs/jls/se7/html/jls-15.html#jls-15.28)    
`A variable of primitive type or type String, that is final and initialized with a compile-time constant expression (§15.28), is called a constant variable.
`

`constant means compile time constant
`

* final variables values cannot be changed but each object of the class will have its own copy of the final variable
* As Static variables are initialized at compile time, we should use static final for constants
https://stackoverflow.com/a/8093375/6465925

##### while and do while
* `while` loop starts with the checking of condition. If it evaluated to true, then the loop body statements are executed otherwise first statement following the loop is executed. For this reason it is also called **Entry control loop**.
* `do while` loop is similar to while loop with only difference that it checks for condition after executing the statements, and therefore is an example of **Exit Control Loop**.


####Useful Links     
* https://www.javaworld.com/article/2073473/java-map-get-and-map-containskey.html
* https://stackoverflow.com/questions/14601016/is-using-java-map-containskey-redundant-when-using-map-get
* https://salesforce.stackexchange.com/questions/179661/map-of-get-vs-containskey-efficient-check
* https://stackoverflow.com/questions/1611735/java-casting-object-to-array-type
* http://tutorials.jenkov.com/java-reflection/methods.html

---
##2.3ThreeSum

####List.toArray()

* Object[] - toArray()  
Returns an array containing all of the elements in this list in proper sequence (from first to last element).

* <T> T[] - toArray(T[] a)Returns an array containing all of the elements in this list in proper sequence (from first to last element); the runtime type of the returned array is that of the specified array.  
https://docs.oracle.com/javase/8/docs/api/java/util/List.html

####Object Arrays
Object arrays cannot be casted to Integer array  
https://stackoverflow.com/questions/1115230/casting-object-array-to-integer-array-error

* Need more understanding on this  

####TypeCasting

Type parent = new Child();  
`(Child)parent` wil return a child object

Type parent = new Parent();  
`(Child)parent` will throw error as the object being created is that of parent
---

##2.4reverseLetter

####Arrays.asList()

* Returns a fixed-size list backed by the specified array, which is an ArrayList implementation

      public static <T> List<T> asList(T... a) {  
                      return new ArrayList<>(a);
                        }
* Changes to the returned list "write through" to the array.[1]

* Write through is a storage method in which data is written into the cache and the corresponding main memory location at the same time. The cached data allows for fast retrieval on demand, while the same data in main memory ensures that nothing will get lost if a crash, power failure, or other system disruption occurs.

#### Initialization of ArrayList 
* Four ways
    * https://www.geeksforgeeks.org/initialize-an-arraylist-in-java/
* ArrayList does not work with primitives
    * https://www.geeksforgeeks.org/array-vs-arraylist-in-java/   
* Note, the difference in array elements while creating an ArrayList
    * It does not uses {}
    
    
        ArrayList<String> gfg = new ArrayList<String>( 
            Arrays.asList("Geeks", 
                          "for", 
                          "Geeks"));    
#### Difference between `Arrays.asList(array)` and `new ArrayList<<T>>(Arrays.asList(array))` [2]

##### Arrays.asList
* Nothing is copied, just a single wrapper object is created.
* Operations on the list wrapper are propagated to the original array.
* Some `List` operations aren't allowed on the wrapper
    * like adding or removing elements from the list.  
    * You can only read or overwrite the elements.
    * UnsupportedOperationException will be thrown while trying to add or delete an element from it [3]
* The wrapper **does not extends ArrayList**

##### new ArrayList<Integer>(Arrays.asList(ia))
* It new ArrayList, which is a full, independent copy of the original one.
* Although here you create the wrapper using Arrays.asList as well, it is used only during the construction of the new ArrayList and is garbage-collected afterwards. 
* The structure of this new ArrayList is completely independent of the original array. 
    * It contains the same elements (both the original array and this new ArrayList reference the same integers in memory)
     * But it creates a new, internal array, that holds the references. 
     * So when you shuffle it, add, remove elements etc., the original array is unchanged.


#### Note [4]
* If passing a primitive array to Arrays.asList(), the size of resulted list will be **1**
    * As we are just passing one array **object** of the primitive to the list
* Always remember, **array is an object**
* Check the output of overloaded method reverseLetterUsingCollection




####Links

* [1] https://docs.oracle.com/javase/7/docs/api/java/util/Arrays.html#asList(T...)
* [2] https://stackoverflow.com/questions/16748030/difference-between-arrays-aslistarray-and-new-arraylistintegerarrays-aslist  
    * [2.1] https://stackoverflow.com/questions/20538869/what-is-the-best-way-of-using-arrays-aslist-to-initialize-a-list
* [3] https://stackoverflow.com/questions/37013151/why-does-arrays-aslist-return-a-fixed-size-list    
* [4] https://stackoverflow.com/questions/21890843/arrays-aslist-size-returns-1   
    * [4.1] https://stackoverflow.com/questions/1248763/arrays-aslist-of-an-array/1248793#1248793
    
---
# TODO

Check fraction and Math Class in Java