package binary;

public class BinarySearch {


    public static void main(String[] args) {
        int[] arr = {2, 4, 5, 6, 7, 8, 9};
        // Question : What is the Time complexity of Arrays.sort
        int elementToSearch = 8;
        int index = binarySearchIterative(elementToSearch, arr);
        System.out.println(index);

        System.out.println("Starting Recursion");
        int index2 = binarySearchRecursive(elementToSearch, arr, 0, arr.length - 1);
        System.out.println(index2);

        int index3 = binarySearchRecursive2(elementToSearch, arr, 0, arr.length - 1);
        System.out.println(index3);
    }


    public static int binarySearchIterative(int element, int[] arr) {
        int low = 0;
        // Caveat : length - 1
        int high = arr.length - 1;
        int mid;
        // Question : Edge cases, should it be + 1 ?
        // Answer : Not +1, but use <= for the edge case of 0 or length - 1
        while (low <= high) {
            /* Note :
             high - low, for the scenario of low of 4 and high = 6 will return 1
             But in the case that right side of array is to be considered, we want the pointer to move 1 after the start of next half
             Hence, use
             low + (high - low) + 2
             */
            mid = low + (high - low) / 2;
            if (arr[mid] > element) {
                high = mid - 1;
            } else if (arr[mid] < element) {
                low = mid + 1;
            } else {
                return mid;
            }
        }
        return -1;
    }


    // Question to mithilesh; why should i return the method
    public static int binarySearchRecursive(int element, int[] arr, int low, int high) {
        int mid = low + (high - low) / 2; // 0 + 3
        if (arr[mid] > element) {
             binarySearchRecursive(element, arr, low, mid - 1);
        } else if (arr[mid] < element) {
             binarySearchRecursive(element, arr, mid + 1, high);
        }
        return mid;
    }

    // Question to mithilesh
    public static int binarySearchRecursive2(int element, int[] arr, int low, int high) {
        // Note : Handle the negative case, element not present in array
        if (high > low)
        {
            // Question : What will happen if i define mide above the if condition ?
            int mid = low + (high - low) / 2; // 0 + 3


            if (arr[mid] > element) {
                return binarySearchRecursive2(element, arr, low, mid - 1);
            } else if (arr[mid] < element) {
                return binarySearchRecursive2(element, arr, mid + 1, high);
            }
            return mid;
        }
        return -1;
    }
}
