package binary;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BinarySearchTest {

    BinarySearch binarySearch;
    int[] arr;

    @Before
    public void setUp() throws Exception {
        binarySearch = new BinarySearch();
        // Note : using int[] arr = {1,2,3} is not allowed here
        arr = new int[]{2, 4, 5, 6, 7,8, 9};
    }

    @After
    public void tearDown() throws Exception {
        binarySearch = null;
    }

    @Test
    public void checkResult()
    {
        assertEquals(0, binarySearch.binarySearchIterative(2, arr));
        assertEquals(1, binarySearch.binarySearchIterative(4, arr));
        assertEquals(2, binarySearch.binarySearchIterative(5, arr));
        assertEquals(3, binarySearch.binarySearchIterative(6, arr));
        assertEquals(4, binarySearch.binarySearchIterative(7, arr));
        assertEquals(5, binarySearch.binarySearchIterative(8, arr));
        assertEquals(6, binarySearch.binarySearchIterative(9, arr));
        assertEquals(-1, binarySearch.binarySearchIterative(19, arr));

    }

}