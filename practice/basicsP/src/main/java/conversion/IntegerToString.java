package conversion;

public class IntegerToString {
    private static int i = 100;
    private static int j = 02100;

    public static void main(String[] args) {

        String str1 = String.valueOf(i);
        // Note : Incorrect Int for int starting with 0
        String str2 = String.valueOf(j);
        System.out.println(str1);
        System.out.println(str2);

        String str3 = String.format("%d",i);
        // Note : Incorrect Int for int starting with 0
        String str4 = String.format("%d",j);
        System.out.println(str3);
        System.out.println(str4);

    }
}
