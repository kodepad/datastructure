* Static methods in interface should have a body
    * static int[] twoNumberSum( int[] array, int targetSum);

* An abstract method is defined only so that it can be overridden in a subclass.
    * However, static methods can not be overridden.
    * Therefore, it is a compile-time error to have an abstract, static method.
    * It's because static methods belongs to a particular class and not to its instance.

* Links
    * https://stackoverflow.com/questions/370962/why-cant-static-methods-be-abstract-in-java