package expections;

public class AnotherClass1 {

  public static void method1() throws MyException {
    // NOTE 2 : No need to add exception in my signature, if we are not throwing exception in the
    // catch block
    int[] newArr = ExceptionHandling.arr;

    try {
      System.out.println(newArr[newArr.length]);
    } catch (ArrayIndexOutOfBoundsException e)
    // NOTE 1 : Always catch an Exception
    {
      throw new MyException("Arrays maximum index will always be 1 less than its length");
    }
  }
}
