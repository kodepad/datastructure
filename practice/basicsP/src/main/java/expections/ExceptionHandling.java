package expections;

public class ExceptionHandling {

  static int[] arr = {1, 2, 3, 4, 5};

  public static void main(String[] args) {
    // Note 4 : Exception must be caught or declared to be thrown
    // Note 3 : Static method direct invocation
    try {
      AnotherClass1.method1();

      // NOTE Question : Should this line still run after throwing exception
      System.out.println("Working after exception");
    } catch (MyException e) {
      e.printStackTrace();
    }
  }
}
