package inheritance;

public class Parent {

    private void foo(){
        System.out.println("Parent foo");
    }

   // public final void doo()
    public void doo()
    {
        System.out.println("Parent.doo");
    }
}
