package inheritance;

public class Driver {

    public static void main(String[] args) {
        Parent p = new Child();
       // p.foo();
        p.doo();
    }
}

// Note : private method cannot be overridden, as no one can see it, not even the

// Note public final method can not be overriden
// private method are implicitly final