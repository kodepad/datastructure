package inheritance;

public class Child extends Parent{

 //   @Override
    private void foo(){
        System.out.println("Child foo");
    }

    public static void main(String[] args) {
        Parent p = new Child();
        ((Child)p).foo();
    }

    @Override
    public void doo()
    {
        System.out.println("Child.doo");
    }
}
