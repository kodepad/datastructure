import java.util.Arrays;

public class BubbleSort2 {

    public static void main(String[] args) {
        int[] input = {2, -2, -6, -10, 10, 4, -8, -1, -8, -4, 7, -4, 0, 9, -9, 0, -9, -9, 8, 1, -4, 4, 8, 5, 1, 5, 0, 0, 2, -10};
        int[] output = bubbleSort(input);
        System.out.println(Arrays.toString(output));
    }


    public static int[] bubbleSort(int[] array) {

        boolean isSorted = false;
        int lastUnsorted = array.length - 1;

        while (!isSorted) {
            isSorted = true;
            for (int x = 0; x < lastUnsorted; x++) {
                if (array[x] > array[x + 1]) {
                    swapPositions(array, x, x + 1);
                    isSorted = false;
                }
            }
            lastUnsorted--;
        }
        return array;
    }

    public static void swapPositions(int[] array, int i, int j) {
        int temp = array[i];
        array[i] = array[j];
        array[j] = temp;

    }
}

