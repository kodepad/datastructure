### Selection Sort
* Find the smallest value in 2nd portion and swap inplace
* Make sure it is smallest value in 2nd portion and not just a smaller value  
`[12 | 5,3,2,11]`   
* swap 2 with 12 and not at 5
* https://www.youtube.com/watch?v=g-PGLbMth_g

### Selection Sort Pseudocode
There are many different ways to sort the cards. Here's a simple one, called selection sort, possibly similar to how you sorted the cards above:

* Find the smallest card. Swap it with the first card.
* Find the second-smallest card. Swap it with the second card.
* Find the third-smallest card. Swap it with the third card.
* Repeat finding the next-smallest card, and swapping it into the correct position until the array is sorted.

This algorithm is called selection sort because it repeatedly selects the next-smallest element and swaps it into place.
* https://www.educative.io/courses/visual-introduction-to-algorithms/jJPoR

---

Selection sort scans the unsorted subarray for the next element to include in the sorted subarray.

This is the idea behind insertion sort. Loop over positions in the array, starting with index 1. Each new position is like the new card handed to you by the dealer, and you need to insert it into the correct place in the sorted subarray to the left of that position. Here's a visualization that steps through that:

### Insertion Sort pseudocode
Now that you know how to insert a value into a sorted subarray, you can implement insertion sort:

* Call insert to insert the element that starts at index 1 into the sorted subarray in index 0.
* Call insert to insert the element that starts at index 2 into the sorted subarray in indices 0 through 1.
* Call insert to insert the element that starts at index 3 into the sorted subarray in indices 0 through 2.
* …
* Finally, call insert to insert the element that starts at index n-1n−1 into the sorted subarray in indices 0 through n−2.