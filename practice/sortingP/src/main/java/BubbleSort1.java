import java.util.Arrays;

public class BubbleSort1 {

    public static void main(String[] args) {
        int[] input = {2, -2, -6, -10, 10, 4, -8, -1, -8, -4, 7, -4, 0, 9, -9, 0, -9, -9, 8, 1, -4, 4, 8, 5, 1, 5, 0, 0, 2, -10};
        int[] output = bubbleSort(input);
        System.out.println(Arrays.toString(output));
    }


    public static int[] bubbleSort(int[] array) {

        // Note : Make sure j = len - 1, else while swapping it might go out of bound
        int j = array.length - 1;

        for (int i = 0; i < j; i++) {
            for (int x = 0; x < j; x++) {
                // Note : Swap between current and next element, (hence < len - 1)
                if (array[x] > array[x + 1]) {
                    swapPositions(array, x, x + 1);
                }
            }
        }
        return array;
    }


    public static void swapPositions(int[] array, int i, int j) {
        int temp = array[i];
        array[i] = array[j];
        array[j] = temp;

    }
}
