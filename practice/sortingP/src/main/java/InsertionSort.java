import java.util.Arrays;

public class InsertionSort {

    public static void main(String[] args) {
        int[] input = {2, -2, -6, -10, 10, 4, -8, -1, -8, -4, 7, -4, 0, 9, -9, 0, -9, -9, 8, 1, -4, 4, 8, 5, 1, 5, 0, 0, 2, -10};
        int[] output = insertionSort(input);
        System.out.println(Arrays.toString(output));
    }


    public static int[] insertionSort(int[] array) {
        // Iterate over the entire array
        // Swapping is also in a loop, which checks for element position before the ith element
        int j = 0;

        // Note : Always handle edge conditions

        if(array.length ==0)
            return new int[]{};

        for(int i = 1; i < array.length; i ++)
        {
            j = i;
            // Note : Compare with previous element, hence j>0 and i = 1
            while (j >0 && array[j] < array[j-1])
            {
                swap(array, j, j-1);
                j--;
            }
        }
        return array;
    }

    public static void swap(int [] array, int i,int j){
        int temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
}
