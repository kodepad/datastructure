import java.util.Arrays;

public class SelectionSort_Incorrect {

    // NOTE : Even though this code sorts the array, its not selection sort. See notes

    public static void main(String[] args) {
        int[] input = {2, -2, -6, -10, 10, 4, -8, -1, -8, -4, 7, -4, 0, 9, -9, 0, -9, -9, 8, 1, -4, 4, 8, 5, 1, 5, 0, 0, 2, -10};
        int[] output = selectionSort(input);
        System.out.println(Arrays.toString(output));
    }


    public static int[] selectionSort(int[] array) {

        // Length - 1 to avoid out of bound for last element
        for (int i = 0; i < array.length - 1; i++) {
            for (int j = i + 1; j < array.length; j++) {
                if (array[j] < array[i]) {
                    swap(array, i, j);
                }
            }
        }
        return array;
    }

    public static void swap(int[] array, int i, int j) {
        int temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
}
