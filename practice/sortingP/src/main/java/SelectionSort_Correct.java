import java.util.Arrays;

public class SelectionSort_Correct {

    // NOTE : Even though this code sorts the array, its not selection sort. See notes

    public static void main(String[] args) {
      //  int[] input = {2, -2, -6, -10, 10, 4, -8, -1, -8, -4, 7, -4, 0, 9, -9, 0, -9, -9, 8, 1, -4, 4, 8, 5, 1, 5, 0, 0, 2, -10};
        int[] input = {3,1,2};
        int[] output = selectionSort(input);
        System.out.println(Arrays.toString(output));
    }


    public static int[] selectionSort(int[] array) {

        // Find the smallest value in 2nd portion and swap inplace
        // Make sure it is smallest value in 2nd portion and not just a smaller value
        // [12 | 5,3,2,11] - swap at 2 and not at 5

        int shortestIndex = 0;

        // Lenght - 1 to avoid exception at end of array
        for(int i =0; i < array.length - 1; i++) {
            shortestIndex = i;
            for(int j = i+1; j < array.length; j++){
                if(array[j] < array[shortestIndex])
                {
                    shortestIndex = j;
                }
                // Caveat : Swap after you find the smallest value index and not the smaller value index
                // Note : Hence no swapping here, swap after inner for loop
                // Using swap here, gives output as [2,1,3]
       //         swap(array, i, shortestIndex);
            }
            swap(array, i, shortestIndex);

        }
        return array;
    }

    public static void swap(int[] array, int i, int j) {
        int temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
}
