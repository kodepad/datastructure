package permutations;

// Write a recursive method for generating all permutations of an input string. Return them as a set.

import common.Practice;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class Permutations {

    public static void main(String args[]){
        driverRecursive();
    }

    public static void driverRecursive()
    {
        String input = "cat";
        Set<String> result = recursivePermutations(input);
        System.out.println(result);

        permutaion(input, 0, input.length() - 1);
    }

   // Question : How to handle duplicate letter scenarios
    @Practice
    private static Set<String> recursivePermutations(String input)
    {
        // Note : Base condition for recursion
        if(input.length()<=1)
        {
            // TODO : Check what collections.singletonList does
            return new HashSet<>(Collections.singletonList(input));
        }

        // Note : Should I have an if else in recursion ? What about returning the same method ?

        char lastCharachter = input.charAt(input.length() - 1);
        Set<String> permutationsOfAllCharsExceptLast = recursivePermutations(input.substring(0, input.length() - 1));

        // Note : Put the last char in all possible positions

        Set<String> permutations = new HashSet<>();
        for(String s : permutationsOfAllCharsExceptLast)
        {
            for(int i = 0; i<= s.length(); i++)
            {
                String s1 = s.substring(0, i) + lastCharachter + s.substring(i);
                permutations.add(s1);
            }

        }

return permutations;

    }


    private static void permutaion(String input, int leftIndex, int rightIndex) {
        // Note : Always check for the end / base condition
        if (leftIndex == rightIndex) {
            System.out.println(input);
        }

        for (int i = leftIndex; i <= rightIndex; i++) {
            String swapped = swapPositions(input, leftIndex, rightIndex);
            permutaion(swapped, leftIndex + 1, rightIndex);
        }
    }

    private static String swapPositions(String input, int l, int r)
    {
        char temp;
        // NOTE : Converting String to CharachterArray
        char [] charArr = input.toCharArray();
        temp = charArr[l];
        charArr[l] = charArr[r];
        charArr[r] = temp;
        // Note : Converting charachter array back to String
        return String.valueOf(charArr);
    }
}
