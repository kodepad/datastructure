package validateBST;

public class Solution2 {
    public boolean isValidBST(TreeNode root) {

        return validateBST(root, Long.MIN_VALUE, Long.MAX_VALUE);
    }

    private boolean validateBST(TreeNode root, long min, long max){
        // base case with no tree i.e is bst

        return root == null ||
                (root.val > min && root.val < max)
                        && validateBST(root.left,min,root.val)  // left subtree from -infinity to root.val
                        && validateBST(root.right,root.val,max);
    }
}