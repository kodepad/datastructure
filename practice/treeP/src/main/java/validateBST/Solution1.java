package validateBST;

// Leetcode # 98

import java.util.LinkedList;
import java.util.List;

public class Solution1 {

    List<Integer> lst = new LinkedList<>();
    List<Integer> inOrderResult = new LinkedList<>();

    public boolean isValidBST(TreeNode root) {

        inOrderResult = traverseInOrder(root, lst);

        for (int i = 1; i < inOrderResult.size(); i++) {
            if (inOrderResult.get(i) <= inOrderResult.get(i - 1))
                return false;
        }
        return true;
    }

    private static List<Integer> traverseInOrder(TreeNode node, List<Integer> lst) {
        if (node != null) {
            traverseInOrder(node.left, lst);
            lst.add(node.val);
            traverseInOrder(node.right, lst);
        }
        return lst;
    }
}
