package binaryTree;


import java.util.LinkedList;

public class Driver {

    // Tree taken from : https://www.geeksforgeeks.org/tree-traversals-inorder-preorder-and-postorder/

    public static void main(String[] args)
    {
        BinaryTree tree1 = new BinaryTree();
        // Note : Not a BST
        tree1.root = new Node("1");
        tree1.root.left = new Node("2");
        tree1.root.right = new Node("3");
        tree1.root.left.left = new Node("4");
        tree1.root.left.right = new Node("5");

        System.out.println("============");
        TreePrinter.print(tree1.root);
        System.out.println("Preorder traversal of binary tree is ");
        tree1.preOrderRecursive(tree1.root);

        System.out.println("\nInorder traversal of binary tree is ");
        tree1.inOrderRecursive(tree1.root);

        System.out.println("\nPostorder traversal of binary tree is ");
        tree1.postOrderRecursive(tree1.root);


        System.out.println("=============");

        LinkedList<String> inOrderResult = new LinkedList<>();
        LinkedList<String> postOrderResult = new LinkedList<>();
        LinkedList<String> preOrderResult = new LinkedList<>();

        System.out.println(tree1.preOrderTraversalList(tree1.root, preOrderResult));
        System.out.println("=============");
        System.out.println(tree1.inOrderTraversalList(tree1.root, inOrderResult));
        System.out.println("=============");
        System.out.println(tree1.postOrderTraversalList(tree1.root, postOrderResult));
        System.out.println("=============");








        // Another example
        BinaryTree tree2 = new BinaryTree();
        // Level 0
        tree2.root = new Node("25");

        // Level 1
        tree2.root.left = new Node("15");
        tree2.root.right = new Node("50");

        // Level 2
        tree2.root.left.left = new Node("10");
        tree2.root.left.right = new Node("22");
        tree2.root.right.left = new Node("35");
        tree2.root.right.right = new Node("70");

        // Level 3
        tree2.root.left.left.left = new Node("4");
        tree2.root.left.left.right = new Node("12");
        tree2.root.left.right.left = new Node("18");
        tree2.root.left.right.right = new Node("24");
        tree2.root.right.left.left = new Node("31");
        tree2.root.right.left.right = new Node("44");
        tree2.root.right.right.left = new Node("66");
        tree2.root.right.right.right = new Node("90");

        System.out.println("============");
        TreePrinter.print(tree2.root);
        System.out.println("Preorder traversal of binary tree is ");
        tree1.preOrderRecursive(tree2.root);

        System.out.println("\nInorder traversal of binary tree is ");
        tree1.inOrderRecursive(tree2.root);

        System.out.println("\nPostorder traversal of binary tree is ");
        tree1.postOrderRecursive(tree2.root);
    }
}

