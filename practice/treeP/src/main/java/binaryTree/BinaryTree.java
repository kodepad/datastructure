package binaryTree;


import java.util.List;

public class BinaryTree {
    Node root;

    public BinaryTree() {
        root = null;
    }


    /*
        Traversals
        Note : The sequence of child would always be LEFT and RIGHT
        CAVEAT : Always check for not null, else NPE
        Using visit method to carry out different tasks such as storing in array
     */

    void preOrderRecursive(Node node) {
        if (node != null) {
            visit(node);
            preOrderRecursive(node.left);
            preOrderRecursive(node.right);
        }
    }

    void inOrderRecursive(Node node) {
        if (node != null) {
            inOrderRecursive(node.left);
            visit(node);
            inOrderRecursive(node.right);
        }
    }

    void postOrderRecursive(Node node) {
        if (node != null) {
            postOrderRecursive(node.left);
            postOrderRecursive(node.right);
            visit(node);
        }
    }

    private void visit(Node node) {
        System.out.println(node.key);
    }


    public List<String> inOrderTraversalList(Node tree, List<String> array) {
        if (tree != null) {
            inOrderTraversalList(tree.left, array);
            array.add(tree.key);
            inOrderTraversalList(tree.right, array);
        }
        return array;
    }

    public List<String> preOrderTraversalList(Node tree, List<String> array) {
        if (tree != null) {
            array.add(tree.key);
            preOrderTraversalList(tree.left, array);
            preOrderTraversalList(tree.right, array);
        }
        return array;
    }

    public List<String> postOrderTraversalList(Node tree, List<String> array) {
        if (tree != null) {
            postOrderTraversalList(tree.left, array);
            postOrderTraversalList(tree.right, array);
            array.add(tree.key);
        }
        return array;
    }
}

// Note : If wrapping Node inside Binary tree, use STATIC CLASS
// For the purposes of interview questions, we typically do not use a Tree class

class Node implements TreePrinter.PrintableNode {
    String key;
    Node left;
    Node right;

    public Node(String key) {
        this.key = key;
        this.left = this.right = null;
    }

    @Override
    public TreePrinter.PrintableNode getLeft() {
        return left;
    }

    @Override
    public TreePrinter.PrintableNode getRight() {
        return right;
    }

    @Override
    public String getText() {
        return key;
    }


}