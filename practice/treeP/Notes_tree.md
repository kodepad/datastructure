## Binary Tree
A tree where each Node has **upto** two children.

### Binary Serach Tree
A binary search tree imposes the condition that, for each node, its left descendents are less than or equal to the current node, which is less than the right descendents.
* rightChild <= parentNode
* leftChild > parentNode
    
            1
        1       2
    
### Balanced Tree
* A tree which is not terribly unbalanced
* Its balanced enough to ensure O(log n) for
    * insert()
    * find()
    
#### Types   
* Red black tree
* AVL tree


### Complete Binary Trees
A complete binary tree is a binary tree in which 
* every level of the tree is fully filled, except for perhaps the last level. 
* To the extent that the last level is filled, it is filled left to right.

### Full Binary Trees
A full binary tree is a binary tree in which 
* every node has either zero or two children. 
* That is, no nodes have only one child.


### Perfect Binary Trees
A perfect binary tree is one that is
* both full and complete.
* All leaf nodes will be at the same level, and this level has the maximum number of nodes.
* A perfect tree must have exactly `2^k - 1` nodes (where k is the number of levels.


### Traversals
* In-Order
    * Left branch, Current Node, Right branch
    * When performed on BST, it visits the nodes in ascending order(hence the name)
* Pre-Order
    * Current Node, Left branch, Right branch
    * In a pre-order traversal, the root is always the first node visited.
* Post-Order
    * Left branch, Right branch, Current Node
    *In a post-order traversal, the root is always the last node visited.
* Note
    * The sequence of child would always be LEFT and RIGHT
    * DO NOT visit or print the node, until no further child or visited nodes 
    * DO NOT FORGET NP CHECK 
    
### BST Condition
* All left nodes must be less than or equal to the current node, which must be less than all the right nodes.

### BFS
* All conncetions at one time
* Use Queue (LinkedList implement Deque which extends Queue)
* End result is same as traversing each level from left to right
* Touch all the nodes of tree

### DFS
* 1 Connection at one time
