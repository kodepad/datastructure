### Adajacency Matrix
* Adjacency Matrix is a n * n matrix, where n is the number of vertices
* If no self referential loop - Diagonals are zero
* Undirected edge - (x,y) = (y,x) = 1

        a[i][j] = 1, if i and j are adjacent
                = 0, otherwise
     
* Space Complexity - n^2
* Use when its a dense graph

### Adjacency List
* Will be a an array of linked list
* For each vertex a linked list will be maintained, whose valud would be the adjacent vertices
* Space Complexity -> V + 2E
* Use in case of sparse graph

#### Links
* https://www.youtube.com/watch?v=5hPfm_uqXmw