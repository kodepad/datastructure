package adjacency;


import java.util.LinkedList;
import java.util.List;

// Graph example using adjacency list


public class AdjacentyList {

  public static void main(String[] args) {
    // Array of 10 linked list
    List<Integer>[] l = new LinkedList[10];

    l[0].add(5);
    l[1].add(0,10);
    System.out.println(l);
  }


}
