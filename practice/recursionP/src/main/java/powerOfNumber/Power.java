package powerOfNumber;

import factorial.FactorialIterative;
import arrayListP.ArrayListInit1;

public class Power {

    public static double exponential(int x, int n) {
        if (n == 0)
            return 1;
        else if(n<0)
            return 1/(x * exponential(x, -n));
        else
        return x * exponential(x, n - 1);
    }
    /**
     *     @see FactorialIterative#factorial(int)
     *     { @link arrayListP.ArrayListInit1#method3_backingArray()}
     *     @see arrayListP.ArrayListInit1#method3_backingArray
     * @see arrayListP.ArrayListInit1
     */

    /**
     *      Refer to {@linkplain arrayListP.ArrayListInit1#method3_backingArray() the overridden method}.
     */
    /**
     *
     * @param args
     * @link FactorialIterative#factorial(int)
     */
    public static void main(String[] args) {
        System.out.println(exponential(2, 3));
        System.out.println(exponential(2, -3));
        System.out.println(exponential(3, 4));
        System.out.println(exponential(5, 3));

    }
}
