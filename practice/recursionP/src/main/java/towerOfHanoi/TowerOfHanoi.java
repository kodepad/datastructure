package towerOfHanoi;

// https://www.youtube.com/watch?v=q6RicK1FCUs

public class TowerOfHanoi {

    public static void TOH(int n, String source, String using, String destination){
        if(n>0) {
            TOH(n - 1, source, destination, using);
            System.out.println("Move a disk from " + source + " " + destination);
            TOH(n-1, using, source, destination);
        }
    }

    public static void main(String[] args) {
        TOH(5,"B","A","C");
    }
}
