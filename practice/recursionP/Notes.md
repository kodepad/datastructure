### Basic idea behind recursive algorithms:
To solve a problem, solve a subproblem that is a smaller instance of the same problem, and then use the solution to that smaller instance to solve the original problem.

* We can distill the idea of recursion into two simple rules:
    * Each recursive call should be on a smaller instance of the same problem, that is, a smaller subproblem.
    * The recursive calls must eventually reach a base case, which is solved without further recursion.