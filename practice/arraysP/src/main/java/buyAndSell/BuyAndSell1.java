package buyAndSell;

public class BuyAndSell1 {

    /*
    LC : 121. Best Time to Buy and Sell Stock - At most one transaction

Say you have an array for which the ith element is the price of a given stock on day i.If you were only permitted to complete at most one transaction (i.e., buy one and sell one share of the stock), design an algorithm to find the maximum profit. You cannot sell a stock before you buy one.
 */

    // Brute Force
    public static int maxProfit1(int prices[]) {
        int maxprofit = 0;
        for (int i = 0; i < prices.length - 1; i++) {
            for (int j = i + 1; j < prices.length; j++) {
                int profit = prices[j] - prices[i];
                if (profit > maxprofit)
                    maxprofit = profit;
            }
        }
        return maxprofit;
    }

    // Peak and valley

    /*
    The points of interest are the peaks and valleys in the given graph.
    We need to find the largest peak following the smallest valley.
    We can maintain two variables - lowestPriceTillThatDay and maxprofit corresponding to the smallest valley and maximum profit
    (maximum difference between selling price and minprice) obtained so far respectively.
     */

    // https://java2blog.com/stock-buy-sell-to-maximize-profit/

    public static int maxProfit2(int prices[]) {
        int lowestPriceTillThatDay;
        int maxProfit = 0;

        if(prices.length==0)
        {
            lowestPriceTillThatDay = 0;
        }
        else{
            lowestPriceTillThatDay = prices[0];
        }

        for (int i = 0; i < prices.length; i++) {
            int profit = 0;
            if (prices[i] > lowestPriceTillThatDay) {
                profit = prices[i] - lowestPriceTillThatDay;
                if (profit > maxProfit) {
                    maxProfit = profit;
                }
            } else {
                lowestPriceTillThatDay = prices[i];
            }
        }
        return maxProfit;
    }

    // LC
    public int maxProfit(int prices[]) {
        int minprice = Integer.MAX_VALUE;
        int maxprofit = 0;
        for (int i = 0; i < prices.length; i++) {
            if (prices[i] < minprice)
                minprice = prices[i];
            else if (prices[i] - minprice > maxprofit)
                maxprofit = prices[i] - minprice;
        }
        return maxprofit;
    }
}

