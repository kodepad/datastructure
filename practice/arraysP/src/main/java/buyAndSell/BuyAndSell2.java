package buyAndSell;

public class BuyAndSell2 {

/*
LC - 122. Best Time to Buy and Sell Stock II  -  No limit on the number of transactions.

Say you have an array prices for which the ith element is the price of a given stock on day i. Design an algorithm to find the maximum profit. You may complete as many transactions as you like (i.e., buy one and sell one share of the stock multiple times).You may not engage in multiple transactions at the same time (i.e., you must sell the stock before you buy again).

Soln ->
 Instead of looking for every peak following a valley, we can simply go on crawling over the slope and keep on adding the profit obtained from every consecutive transaction. In the end,we will be using the peaks and valleys effectively, but we need not track the costs corresponding to the peaks and valleys along with the maximum profit,

 but we can directly keep on adding the difference between the consecutive numbers of the array if the second number is larger than the first one,

 and at the total sum we obtain will be the maximum profit. This approach will simplify the solution. This can be made clearer by taking this example:
*/

    // Check the difference between i+1 th day and i th day. If its positive, keep on adding

    public static int maxProfitUnlimitedTx(int[] prices) {
        int maxprofit = 0;
        // Note : i = 1 here
        for (int i = 1; i < prices.length; i++) {
            if (prices[i] > prices[i - 1])
                maxprofit += prices[i] - prices[i - 1];
        }
        return maxprofit;
    }
}
