####List.toArray()

* Object[] - toArray()  
Returns an array containing all of the elements in this list in proper sequence (from first to last element).

* <T> T[] - toArray(T[] a)Returns an array containing all of the elements in this list in proper sequence (from first to last element); the runtime type of the returned array is that of the specified array.  
https://docs.oracle.com/javase/8/docs/api/java/util/List.html

####Object Arrays
Object arrays cannot be casted to Integer array  
https://stackoverflow.com/questions/1115230/casting-object-array-to-integer-array-error

* Need more understanding on this  


####TypeCasting

Type parent = new Child();  
`(Child)parent` wil return a child object

Type parent = new Parent();  
`(Child)parent` will throw error as the object being created is that of parent