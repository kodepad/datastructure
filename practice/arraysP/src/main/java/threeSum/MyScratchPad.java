package threeSum;


import common.Practice;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MyScratchPad {
    private static int[] input1 = {12, 3, 1, 2, -6, 5, -8, 6, 1};

    //    private static int[] input1 = {12, 3, 1, 2, -6, 5, -8, 6};
    private static int targetSum = -13;

    private static String methodInExecution;

    public static void main(String args[]) {
        try {
            for (Method method : MyScratchPad.class.getMethods()) {
                if (method.isAnnotationPresent(Practice.class)) {
                    Object returnedObject = method.invoke(null, input1, targetSum);
                    int[] result = (int[]) returnedObject;
                    printResult(result, getMethodInExecution());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private static void printResult(int[] result1, String methodInExecution) {
        System.out.print(methodInExecution);
        for (int i : result1) {
            System.out.print("\t" + i + "\t");
        }
        System.out.println();
    }


    //  Question 1 :Object array to Integer array
    // Question 2 : Can i return 2d array from an object array
//    @Practice
    public static Integer[] bruteForce_1(int[] array, int targetSum) {
        setMethodInExecution(Thread.currentThread().getStackTrace()[1].getMethodName());

        int [] res = new int[10];
        //   int [] result;
        List<Integer []> lst = new ArrayList();
        //      int[][] arr;

        Arrays.sort(array);

        for(int i = 0; i < array.length; i++){
            for(int j = 1; j < array.length; j++){
                for (int k = 2; k < array.length; k++){
                    if(array[i] + array[j] + array[k] == targetSum){
                        lst.add(new Integer[] {array[i], array[j], array[k]});
                    }
                }
            }
        }
        //   Integer [] result = lst.toArray(new Integer[10]);
        Integer [] obj = lst.toArray(new Integer[1]);
        //     Integer[] obj1 = (Integer[]) obj;
        //    int[][] obj2 = (int[][]) obj;
        return obj;
    }


    @Practice
    public static Integer[] bruteForce_2(int[] array, int targetSum) {
        setMethodInExecution(Thread.currentThread().getStackTrace()[1].getMethodName());

        List<Integer []> lst = new ArrayList();
        int count = 0;
        Arrays.sort(array);
        for(int i = 0; i < array.length; i++) {
            for (int j = 1; j < array.length; j++) {
                for (int k = 2; k < array.length; k++) {
                    if (array[i] + array[j] + array[k] == targetSum) {
                        lst.add(new Integer[]{array[i], array[j], array[k]});
                        count++;
                    }
                }
            }
        }
        System.out.println("convertin");
        Object obj [] = lst.toArray();
        Integer [] res = new Integer[count];

        for (int i =0; i < obj.length;  i++)
        {
            res[i] = (Integer) obj[i];

            //       res[i][0] = (int [][])obj[i];
        }

        //Integer[] integerArray = Arrays.copyOf(a, a.length, Integer[].class);
        System.out.println(res.toString());
        return res;
    }


    public static String getMethodInExecution() {
        return methodInExecution;
    }

    public static void setMethodInExecution(String methodInExecution) {
        MyScratchPad.methodInExecution = methodInExecution;
    }

}
