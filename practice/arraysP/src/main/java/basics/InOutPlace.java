package basics;

import java.util.Arrays;

public class InOutPlace {

    public static void main(String[] args) {
        int[] arr = {1,2,3,4,5};
        squareArrayInPlace(arr);
        System.out.println(Arrays.toString(arr));
        int newArr[] = squareArrayOutOfPlace(arr);
        System.out.println(Arrays.toString(newArr));
        System.out.println(Arrays.toString(arr));
    }

    // Note : InPlace - No need to copy, modify the original data

    public static void squareArrayInPlace(int[] intArray) {

        for (int i = 0; i < intArray.length; i++) {
            intArray[i] *= intArray[i];
        }

        // NOTE: no need to return anything - we modified intArray in place
    }

    // Note : Out Of Place - Make a copy and work on copy

    public static int[] squareArrayOutOfPlace(int[] intArray) {

        // Note : we allocate a new array with the length of the input array
        int[] squaredArray = new int[intArray.length];

        for (int i = 0; i < intArray.length; i++) {
            squaredArray[i] = (int) Math.pow(intArray[i], 2);
        }

        return squaredArray;
    }
}
