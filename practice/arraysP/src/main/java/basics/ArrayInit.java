package basics;

import java.util.Arrays;

public class ArrayInit {

    // Initializing during declaration
    // Note : When assigning values to an array during initialization, the size is not specified.
    private static int[] arr1 = {1, 2, 3, 4, 5};


    // Just declaration:
    private static int[] arr2;

    private static int[] arr3 = new int[]{100, 200, 300};

    // Initializing an array, without assigning a value
    private static int[] arr4 = new int[5];

    public static void main(String[] args) {
        System.out.println("Array initialized during declaration");
        printArr(arr1);
        System.out.println("Printing just a declared array, unassigned");
        printArr(arr2);
        System.out.println("Array initialized with new operator during declaration");
        printArr(arr3);

        System.out.println("Printing an array which is not assigned any value yet");
        printArr(arr4);
        // Note: Assigning value to unassigned initialized array
        initArr4();
        // Note : Not allowed here, throws compile time error


        System.out.println("Assigning value to just a declared array. Use of new() is mandatory");
        //    arr2 = {10,20,30,40};
/*        You can only use the abbreviated syntax `int[] pos = {0,1,2};` at variable initialization time.
        At other places, we should use `pos = new int[]{1,2,3};`
        */

        // Note : Wrong syntax, not like constructor
//        arr2 = new int[4] (10,20,30,40);

        // Note : correct syntax
        arr2 = new int[]{10, 20, 30, 40};
        System.out.println(Arrays.toString(arr2));


        // Note : Shortcut is allowed inside a method
        System.out.println("Array can be declared and initialized inside a method too");
        int[] arr5 = {11, 22, 33, 44, 55};
        printArr(arr5);

    }

    private static void initArr4() {
        System.out.println("Assigning value after declaration and default initialization");
        for (int i = 0; i < arr4.length; i++) {
            arr4[i] = i * i;
        }
        printArr(arr4);
    }

    private static void printArr(int[] arr) {
        System.out.println(Arrays.toString(arr));
    }
}
