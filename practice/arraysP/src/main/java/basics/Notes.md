#### Array Initialization

* https://www.geeksforgeeks.org/arrays-in-java/
* https://www.educative.io/edpresso/how-to-initialize-an-array-in-java
* https://www.journaldev.com/750/initialize-array-java#initializing-an-array-in-java-8211-primitive-type
* https://www.geeksforgeeks.org/default-array-values-in-java/
* https://www.baeldung.com/java-initialize-array


You can only use the abbreviated syntax `int[] pos = {0,1,2};` at variable initialization time.
At other places, we should use `pos = new int[]{1,2,3};`

* https://stackoverflow.com/questions/41658497/array-initializer-is-not-allowed-here

To print the element of arrays, use  
`Arrays.toString(arr);`

Always for an index equality, use   
`MaxIndex = arr.length - 1`


For primitive types, the array will be initialized with the default values (0 for int).  
You can make it `Integer [] theArray = new Integer[20]`, it will have null for unassigned values

ArrayList has a size(), Arrays / arrays do not have one [1]

`System.arraycopy` can be used to copy some elements from one array to another [2]

Always use `Integer[]` in coding 
int[] vs Integer[]  [2.1]


Array vs Arrays [3][3.1]

Arrays pass by reference or pass by value [4] [5]

[1] https://stackoverflow.com/questions/4441099/how-do-you-count-the-elements-of-an-array-in-java  
[2] https://docs.oracle.com/javase/7/docs/api/java/lang/System.html#arraycopy
[2.1] https://www.techiedelight.com/convert-int-array-integer-array-java/
[2.2] https://stackoverflow.com/questions/3571352/how-to-convert-integer-to-int
[2.3] https://stackoverflow.com/questions/13747859/how-to-check-if-an-int-is-a-null
[2.4] https://stackoverflow.com/questions/45278904/checking-integer-wrapper-against-null-as-well-as-primitive-value-0
[2.5] https://stackoverflow.com/questions/41183668/how-to-check-whether-an-integer-is-null-or-zero-in-java

[3] https://stackoverflow.com/questions/35760899/why-we-have-arrays-and-array-in-java
[3.1] http://tutorials.jenkov.com/java/arrays.html#inserting-elements-into-an-array
[4] https://stackoverflow.com/questions/12757841/are-arrays-passed-by-value-or-passed-by-reference-in-java
[5] https://stackoverflow.com/questions/21653048/changing-array-in-method-changes-array-outside
[6] https://stackoverflow.com/questions/40480/is-java-pass-by-reference-or-pass-by-value


#### InPlace and OutPlace

##### InPlace
 An in-place method modifies data structures or objects outside of its own stack frame (i.e.: stored on the process heap or in the stack frame of a calling function). Because of this, the changes made by the method remain after the call completes.
    
##### OutOfPlace
 An out-of-place method doesn't make any changes that are visible to other methods. Usually, those methods copy any data structures or objects before manipulating and changing them.    
 
 
Working in-place is a good way to save time and space. An in-place algorithm avoids the cost of initializing or copying data structures, and it usually has an O(1)O(1) space cost.

But be careful: an in-place algorithm can cause side effects. Your input is "destroyed" or "altered," which can affect code outside of your method. For example:

**Generally, out-of-place algorithms are considered safer because they avoid side effects.** You should only use an in-place algorithm if you're space constrained or you're positive you don't need the original input anymore, even for debugging.