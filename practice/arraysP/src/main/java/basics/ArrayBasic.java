package basics;

import java.util.Arrays;

public class ArrayBasic {

    public static void main(String[] args) {

        Integer[] inputArray = new Integer[10];
        Integer[] arr = {1, 20, 5, 78, 5, 30};
        Integer[] newArray;

        // Note : Check the performance of System.arraycopy
        System.arraycopy(arr, 0, inputArray, 0, arr.length);

        // Caveat : int [] would be all initialized with 0, Integer[] will be with null
        // Question : For an int[] how do I know if 0 is an element in array or the end of array

        printArray(0, inputArray);

        newArray = insertElementAtIndex(100, 2, findSizeOfArray(inputArray), inputArray);
        printArray(1,newArray);

        newArray = insertElementAtIndex(44, 3, findSizeOfArray(newArray), newArray);
        printArray(1,newArray);

        int element = 100;
        // Question : Passing inputArray here is taking newArray as input.. Is it same as pass by value or pass by reference
        System.out.println("is " + element + " Present : " + searchElement(element, inputArray));

        element = 10;
        System.out.println("is " + element + " Present : " + searchElement(element, inputArray));

        removeElementFromArray_UniqueElement(100, findSizeOfArray(inputArray), inputArray);
        printArray(1,inputArray);

        removeElementFromArray_DuplicateElement(5, findSizeOfArray(inputArray),inputArray);
        printArray(1,inputArray);
    }

    private static void removeElementFromArray_UniqueElement(int element, int size, Integer[] arr1) {
        if (searchElement(element, arr1)) {
            // this will only find the index of first occurence

            int index = findIndexOfElement(element, arr1);

            for (int i = index; i < size; i++) {
                arr1[i] = arr1[i + 1];
            }
        } else {
            System.out.println(element + "not found");
        }
    }

    private static void removeElementFromArray_DuplicateElement(int element, int size, Integer[] inputArray) {
        for (int i = 0; i < size  ; i++)
        {
            if(inputArray[i] == element)
            {
                int j = i;

                while(j<size-1) {
                    inputArray[j] = inputArray[j + 1];
                    j++;
                }
                inputArray[size - 1] = null;
                size = size - 1;
            }
        }
    }

    private static int findIndexOfElement(int element, Integer[] inputArray) {
        for (int i = 0; i < inputArray.length; i++) {
            if (inputArray[i] == element) {
                return i;
            }
        }
        return Integer.parseInt(null);
    }

    private static boolean searchElement(int element,Integer[] inputArray) {
        for (Integer i : inputArray) {
            if (i != null && i == element) {
                return true;
            }
        }
        return false;
    }


    private static int findSizeOfArray(Integer[] inputArray) {
        int i;
        for (i = 0; i < inputArray.length; i++) {
            if (inputArray[i] == null)
                break;
        }
        return i;
    }


    private static Integer[] insertElementAtIndex(int element, int index, int size, Integer[] inputArray) {

        for (int i = size; i >= index; i--) {
            inputArray[i + 1] = inputArray[i];
        }
        inputArray[index] = element;
        return inputArray;
    }


    private static void printArray(int flag, Integer[] inputArray) {
        // Caveat : Do not print an array directly
        if (flag == 0) {
            System.out.println("Just printing array " + "\t" + inputArray);
            System.out.println("Printing array.toString " + "\t" + inputArray.toString());
            System.out.println("Printing Arrays.toString(arr) " + "\t" + Arrays.toString(inputArray));
            System.out.println(inputArray[6]);
            System.out.println("Printing array \n\n");
        }
        System.out.println(Arrays.toString(inputArray));
    }
}
