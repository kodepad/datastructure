####Arrays.asList()

* Returns a fixed-size list backed by the specified array, which is an ArrayList implementation

      public static <T> List<T> asList(T... a) {  
                      return new ArrayList<>(a);
                        }
* Changes to the returned list "write through" to the array.[1]

* Write through is a storage method in which data is written into the cache and the corresponding main memory location at the same time. The cached data allows for fast retrieval on demand, while the same data in main memory ensures that nothing will get lost if a crash, power failure, or other system disruption occurs.

#### Initialization of ArrayList 
* Four ways
    * https://www.geeksforgeeks.org/initialize-an-arraylist-in-java/
* ArrayList does not work with primitives
    * https://www.geeksforgeeks.org/array-vs-arraylist-in-java/   
* Note, the difference in array elements while creating an ArrayList
    * It does not uses {}
    
    
        ArrayList<String> gfg = new ArrayList<String>( 
            Arrays.asList("Geeks", 
                          "for", 
                          "Geeks"));    
#### Difference between `Arrays.asList(array)` and `new ArrayList<<T>>(Arrays.asList(array))` [2]

##### Arrays.asList
* Nothing is copied, just a single wrapper object is created.
* Operations on the list wrapper are propagated to the original array.
* Some `List` operations aren't allowed on the wrapper
    * like adding or removing elements from the list.  
    * You can only read or overwrite the elements.
    * UnsupportedOperationException will be thrown while trying to add or delete an element from it [3]
* The wrapper **does not extends ArrayList**

##### new ArrayList<Integer>(Arrays.asList(ia))
* It new ArrayList, which is a full, independent copy of the original one.
* Although here you create the wrapper using Arrays.asList as well, it is used only during the construction of the new ArrayList and is garbage-collected afterwards. 
* The structure of this new ArrayList is completely independent of the original array. 
    * It contains the same elements (both the original array and this new ArrayList reference the same integers in memory)
     * But it creates a new, internal array, that holds the references. 
     * So when you shuffle it, add, remove elements etc., the original array is unchanged.


#### Note [4]
* If passing a primitive array to Arrays.asList(), the size of resulted list will be **1**
    * As we are just passing one array **object** of the primitive to the list
* Always remember, **array is an object**
* Check the output of overloaded method reverseLetterUsingCollection




####Links

* [1] https://docs.oracle.com/javase/7/docs/api/java/util/Arrays.html#asList(T...)
* [2] https://stackoverflow.com/questions/16748030/difference-between-arrays-aslistarray-and-new-arraylistintegerarrays-aslist  
    * [2.1] https://stackoverflow.com/questions/20538869/what-is-the-best-way-of-using-arrays-aslist-to-initialize-a-list
* [3] https://stackoverflow.com/questions/37013151/why-does-arrays-aslist-return-a-fixed-size-list    
* [4] https://stackoverflow.com/questions/21890843/arrays-aslist-size-returns-1   
    * [4.1] https://stackoverflow.com/questions/1248763/arrays-aslist-of-an-array/1248793#1248793
    