package reverseLetterInPlace;

// Question : Write a method that takes an array of characters and reverses the letters in place.

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ReverseLetter {

    public static void main(String[] args) {
        driver_forLoop();
        driver_pointers();
        driver_collections_1();
        driver_collections_2();
    }


    private static void reverseLetter(char[] charArr) {
        char temp;
        // Note : If giving length - 1 in second block, use <=
        // Iteration should stop at the middle of array

        for (int i = 0; i <= (charArr.length - 1) / 2; i++) {
            temp = charArr[i];

            // Note : Never forget length - 1
            charArr[i] = charArr[charArr.length - 1 - i];
            charArr[charArr.length - 1 - i] = temp;
        }
    }

    private static void reverseLetterUsingPointers(char[] charArr) {

        // Caveat : Make sure right and left pointers are initialized correctly

        int leftIndex = 0;
        int rightIndex = charArr.length - 1;
        char temp;

        while (leftIndex < rightIndex) {
            temp = charArr[leftIndex];
            charArr[leftIndex] = charArr[rightIndex];
            charArr[rightIndex] = temp;
            // Note : DO NOT FORGET TO INCREMENT DECREMENT COUNTERS INSIDE THE WHILE LOOP ONLY
            // moving toward middle
            leftIndex++;
            rightIndex--;
        }
    }

    private static void reverseLetterUsingCollection(Character[] charArr) {
        List lst = Arrays.asList(charArr);
        Collections.reverse(lst);
        System.out.println("ArrayList size is " + lst.size());
        System.out.println("ArrayList toString is " + lst.toString());
        System.out.println("Iteration ArrayList");
        for (Object c : lst) {
            System.out.println(c);
        }
    }


    private static void reverseLetterUsingCollection(char[] charArr) {
        List lst = Arrays.asList(charArr);
        Collections.reverse(lst);
        System.out.println("ArrayList size is " + lst.size());
        System.out.println("ArrayList toString is " + lst.toString());
        System.out.println("Iteration ArrayList");
        for (Object c : lst) {
            System.out.println(c);
        }
    }

    public static void driver_collections_1() {
        // Reversing array using collection
        System.out.println("Reversing using collection for a char[]");

        char[] oddLengthInput3 = {'i', 'n', 'p', 'u', 't'};
        reverseLetterUsingCollection(oddLengthInput3);
        System.out.println("Arrays.toString() is \t " + Arrays.toString(oddLengthInput3));

        char[] evenLengthInput3 = {'1', '2', '3', '4', '5', '6'};
        reverseLetterUsingCollection(evenLengthInput3);
        System.out.println("Arrays.toString() is \t " + Arrays.toString(evenLengthInput3));
    }

    public static void driver_collections_2() {
        // Reversing array using collection
        System.out.println("Reversing using collection for a Character[]");

        Character[] oddLengthInput3 = {'i', 'n', 'p', 'u', 't'};
        reverseLetterUsingCollection(oddLengthInput3);
        System.out.println("Arrays.toString() is \t " + Arrays.toString(oddLengthInput3));

        Character[] evenLengthInput3 = {'1', '2', '3', '4', '5', '6'};
        reverseLetterUsingCollection(evenLengthInput3);
        System.out.println("Arrays.toString() is \t " + Arrays.toString(evenLengthInput3));
    }

    private static void driver_pointers() {

        // Checking even and odd length input, while using pointers with while loop
        // Using new arrays as previous inputs has been modified using inplace operation

        System.out.println("Reversing using pointers with while loop");
        char[] oddLengthInput2 = {'i', 'n', 'p', 'u', 't'};
        reverseLetterUsingPointers(oddLengthInput2);
        System.out.println(Arrays.toString(oddLengthInput2));

        char[] evenLengthInput2 = {'1', '2', '3', '4', '5', '6'};
        reverseLetterUsingPointers(evenLengthInput2);
        System.out.println(Arrays.toString(evenLengthInput2));
    }

    private static void driver_forLoop() {
        // Checking even and odd length input, while using for loop
        char[] oddLengthInput = {'i', 'n', 'p', 'u', 't'};
        System.out.println("Reversing using for loop till middle");
        reverseLetter(oddLengthInput);
        System.out.println(Arrays.toString(oddLengthInput));
        char[] evenLengthInput = {'1', '2', '3', '4', '5', '6'};
        reverseLetter(evenLengthInput);
        System.out.println(Arrays.toString(evenLengthInput));
    }

}
