##### Setting custom pattern such  as TODO in  Idea
https://www.jetbrains.com/help/idea/using-todo.html


##### Array

`An object is a class instance or an array.
`

`The reference values (often just references) are pointers to these objects, and a special null reference, which refers to no object.
`

`Arrays are objects, are dynamically created, and may be assigned to variables of type Object, All methods of class Object may be invoked on an array.
`

`Every array has an associated Class object, shared with all other arrays with the same component type.
`


* An array is an object in Java
* Every Type in java has an implicit class for the array of Type
* These classes are not accessible for end users
* Object class is the supertype of an array

####Useful Links
* https://docs.oracle.com/javase/specs/jls/se7/html/jls-10.html
* https://docs.oracle.com/javase/specs/jls/se7/html/jls-4.html#jls-4.3.1
* https://docs.oracle.com/javase/specs/jls/se7/html/jls-10.html#jls-10.8

##### Map.get(key)
* Returns the value to which the specified key is mapped, or null if this map contains no mapping for the key.
* NullPointerException if the specified key is null and this map does not permit null keys
        

##### Map.containsKey(key) 

* Will return true or false based on whether key is present or not
* If the specified key is null and this map does not permit null keys 

##### Return ternary operation expression

Java ternary operator is an expression, and is thus evaluated to a single value

`return expression-1 ? expression-2 : expression-3`

where 
* expression-1 has type boolean and 
* expression-2 and expression-3 have the same type.
* https://stackoverflow.com/a/25072826/6465925

##### final

* The way the final keyword works in Java is that the variable's pointer to the value cannot change. Let's repeat that: it's the pointer that cannot change the location to which it's pointing.
    * There's no guarantee that the object being referenced will stay the same
    * only that the variable will always hold a reference to the same object. 
    * If the referenced object is mutable (i.e. has fields that can be changed), then the constant variable may contain a value other than what was originally assigned. 
    * https://www.thoughtco.com/constant-2034049

* If a final variable holds a reference to an object, then the state of the object may be changed by operations on the object, but the variable will always refer to the same object.  
https://docs.oracle.com/javase/specs/jls/se7/html/jls-4.html#jls-4.12.4

##### constant

As per java specs (https://docs.oracle.com/javase/specs/jls/se7/html/jls-15.html#jls-15.28)    
`A variable of primitive type or type String, that is final and initialized with a compile-time constant expression (§15.28), is called a constant variable.
`

`constant means compile time constant
`

* final variables values cannot be changed but each object of the class will have its own copy of the final variable
* As Static variables are initialized at compile time, we should use static final for constants
https://stackoverflow.com/a/8093375/6465925

##### while and do while
* `while` loop starts with the checking of condition. If it evaluated to true, then the loop body statements are executed otherwise first statement following the loop is executed. For this reason it is also called **Entry control loop**.
* `do while` loop is similar to while loop with only difference that it checks for condition after executing the statements, and therefore is an example of **Exit Control Loop**.


####Useful Links     
* https://www.javaworld.com/article/2073473/java-map-get-and-map-containskey.html
* https://stackoverflow.com/questions/14601016/is-using-java-map-containskey-redundant-when-using-map-get
* https://salesforce.stackexchange.com/questions/179661/map-of-get-vs-containskey-efficient-check
* https://stackoverflow.com/questions/1611735/java-casting-object-to-array-type
* http://tutorials.jenkov.com/java-reflection/methods.html
