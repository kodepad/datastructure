package twoSum;

import common.Practice;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class MyScratchPad {
    private static int[] input1 = {3, 5, -4, 8, 11, 1, -1, 6};
    private static int[] input2 = {3, 5, 8, 11, 1, 6};
    private static int targetSum = 10;
    private static int targetSum2 = 15;

    private static String methodInExecution;

    public static void main(String args[]) {
        // Note : Using reflection to run all methods in one go
        // main();
        try {
            for (Method method : MyScratchPad.class.getMethods()) {
                if (method.isAnnotationPresent(Practice.class)) {
                    Object returnedObject = method.invoke(null, input1, targetSum);
                    // Note :No combination scenario : Throwing excpetion for pointers_2
                    // Object returnedObject = method.invoke(null, input1, targetSum2);

                    // Typecast to int []
                    int[] result = (int[]) returnedObject;
                    printResult(result, getMethodInExecution());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private static void printResult(int[] result1, String methodInExecution) {
        System.out.print(methodInExecution);
        for (int i : result1) {
            System.out.print("\t" + i + "\t");
        }
        System.out.println();
    }


    @Practice
    public static int[] bruteForce(int[] array, int targetSum) {
        setMethodInExecution(Thread.currentThread().getStackTrace()[1].getMethodName());

        // Question : Are my indexes correct ? In the pdf, both loops are iterating till n
        for (int i = 0; i < array.length - 1; i++) {
            for (int j = i + 1; j < array.length; j++) {
                if (array[i] + array[j] == targetSum)
                    return new int[]{array[i], array[j]};
            }
        }
        return new int[]{};
    }

    @Practice
    public static int[] hashing_1(int[] array, int targetSum) {
        setMethodInExecution(Thread.currentThread().getStackTrace()[1].getMethodName());

        Map<Integer, Boolean> map = new HashMap<>();

        for (int i : array) {
            map.put(i, true);
        }

        for (int i : array) {
            // Note : get() will return null if now value if found for key
            // Caveat 2

            if (map.get(targetSum - i) != null && map.get(targetSum - i))
                return new int[]{i, targetSum - i};
        }
        return new int[]{};
    }

    @Practice
    public static int[] hashing_2(int[] array, int targetSum) {

        setMethodInExecution(Thread.currentThread().getStackTrace()[1].getMethodName());

        Map<Integer, Boolean> map = new HashMap<>();

        for (int i : array) {
            map.put(i, true);
            // Note : Breaking at (target sum / 2) again, as we did a get after put
            // Caveat : First get and then put

            if (map.get(targetSum - i) != null && map.get(targetSum - i))
                return new int[]{i, targetSum - i};
        }
        return new int[]{};
    }


    @Practice
    public static int[] hashing_3(int[] array, int targetSum) {

        setMethodInExecution(Thread.currentThread().getStackTrace()[1].getMethodName());

        Map<Integer, Boolean> map = new HashMap<>();

        for (int i : array) {
            if (map.get(targetSum - i) != null && map.get(targetSum - i))
                // CAVEAT : This condition is just checking if the key is present or not (through whether get key is true). Could have used containsKey()
                return new int[]{i, targetSum - i};
            map.put(i, true);
        }
        return new int[]{};
    }


    // Note : Forgot to use a loop
    // Question : Can this be solved using a For loop ?
    @Practice
    public static int[] pointers_1(int[] array, int targetSum) {
        setMethodInExecution(Thread.currentThread().getStackTrace()[1].getMethodName());

        Arrays.sort(array);
        int leftPointer = 0;
        int rightPointer = array.length - 1;

        if (array[leftPointer] + array[rightPointer] > targetSum)
            leftPointer++;
        else if (array[leftPointer] + array[rightPointer] < targetSum)
            rightPointer--;
        return new int[]{array[leftPointer], array[rightPointer]};
    }




    // Note : Will only pass if the target sum is present in input, else the pointers will keep moving.
    // Note : Need to put an exit condition for negative scenario
    // Question : How can I make the rest of code execute after an exception is caught
    @Practice
    public static int[] pointers_2(int[] array, int targetSum) {
        setMethodInExecution(Thread.currentThread().getStackTrace()[1].getMethodName());

        Arrays.sort(array);
        int leftPointer = 0;
        int rightPointer = array.length - 1;

        while (array[leftPointer] + array[rightPointer] != targetSum) {
            if (array[leftPointer] + array[rightPointer] < targetSum)
                leftPointer++;
            else
                rightPointer--;
        }
        return new int[]{array[leftPointer], array[rightPointer]};
        // Question : How to handle return of an empty array here ?
    }


    @Practice
    public static int[] pointers_3(int[] array, int targetSum) {
        setMethodInExecution(Thread.currentThread().getStackTrace()[1].getMethodName());

        Arrays.sort(array);
        int rightIndex = array.length - 1;
        int leftIndex = 0;

        while (array[rightIndex] + array[leftIndex] != targetSum && rightIndex != leftIndex) {
            if (array[rightIndex] + array[leftIndex] > targetSum) {
                rightIndex--;
            } else {
                leftIndex++;
            }
        }
        /* Note : Incorrect Syntax
        rightIndex==leftIndex ?  return new int[]{} :  return new int[]{array[leftIndex], array[rightIndex]};
        */
        return rightIndex == leftIndex ? new int[]{} : new int[]{array[leftIndex], array[rightIndex]};
    }



    public static String getMethodInExecution() {
        return methodInExecution;
    }

    public static void setMethodInExecution(String methodInExecution) {
        MyScratchPad.methodInExecution = methodInExecution;
    }

    // overloaded main method
    public static void main() {
        // Note : Using reflection to run all methods in one go
        try {
            MyScratchPad obj = new MyScratchPad();
            Method[] allMethods = {
                    obj.getClass().getMethod("bruteForce", new Class[]{int[].class, int.class}),
                    obj.getClass().getMethod("hashing_1", new Class[]{int[].class, int.class}),
                    obj.getClass().getMethod("hashing_2", new Class[]{int[].class, int.class}),
                    obj.getClass().getMethod("hashing_3", new Class[]{int[].class, int.class})
            };

            for (Method m : allMethods) {
                Object returnedObject = m.invoke(obj, input1, targetSum);
                // Typecast to int []
                int[] result = (int[]) returnedObject;
                printResult(result, getMethodInExecution());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
