// LC # 1099

package twoSumLessThanK;

import java.util.Arrays;

public class MyScratchPad {

    public int twoSumLessThanK(int[] A, int K) {
        // Complexity : O(n log n)
        Arrays.sort(A);
        int low = 0;
        int high = A.length - 1;
        int maxSum = -1;

        // Complexity : O(n)
        while (low < high) {
            if(A[low] + A[high] <K){
                maxSum = Math.max(maxSum, A[low] + A[high]);
                low++;
            }
            else
            {
                high--;
            }
        }
        return maxSum;
    }
}
