package arrayListP;

import java.util.*;

public class ListInit {

    // Note : Always use generics

    public static void main(String[] args) {
        impl1();
        impl2();
        impl3();
        impl4();
    }

    private static void impl1() {
        List<Integer> linkedList = new LinkedList<>();
        linkedList.add(null);
        linkedList.add(1);
        linkedList.add(2);
        linkedList.add(3);
        System.out.println("Linked List");
        System.out.println(linkedList);
        System.out.println("Size " + linkedList.size());
        linkedList.listIterator();
    }

    private static void impl2() {
        List<Integer> arrayList = new ArrayList<>();
        arrayList.add(1);
        arrayList.add(null);
        arrayList.add(2);
        arrayList.add(3);
        System.out.println("Array List");
        System.out.println(arrayList);
    }

    private static void impl3() {
        List<Integer> stack = new Stack<>();
        stack.add(1);
        stack.add(2);
        stack.add(null);
        stack.add(3);
        System.out.println("Stack");
        System.out.println(stack);
    }

    private static void impl4() {
        List<Integer> vector = new Vector<>();
        vector.add(1);
        vector.add(2);
        vector.add(3);
        // Question : Where will this lastIndexOf method be referring to ? that of list or vector ?
        vector.lastIndexOf(2);
        vector.add(null);

        System.out.println("Vector");
        System.out.println(vector);

        // Question : why is the private method grow not visible to vector object
        // Question : Can we inherit a private method
        Vector v = new Vector();
        v.lastIndexOf(2);
       // v.grow
    }
}
