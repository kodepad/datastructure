package arrayListP;

import java.util.Arrays;
import java.util.List;

public class ArrayListInit1 {

/*
Class to show how Array.asList() is just a representation of backing array
It does not have an array of its own
 */
    public static void main(String[] args) {
        method1_intArray();
        method2_ObjectArray();
        method3_backingArray();
    }

    private static void method3_backingArray() {
        System.out.println("Method 3");
        Integer [] arr;
        // this does not work
    //   arr = {10,20,30,40};

        arr = new Integer[]{10,20,30,40};
        List<Integer> lst = Arrays.asList(arr);
        try{
            lst.add(50);
        }catch (UnsupportedOperationException e){
            System.out.println("Add operation is not supported by this list");
        }
        try{
            // Removing the element at third index
            lst.remove(3);
        }catch (UnsupportedOperationException e){
            System.out.println("Remove operation is not supported by this list");
        }
        try{
            System.out.println("Overwriting the element at third index");
            lst.set(3,99);
        }catch (UnsupportedOperationException e){
            System.out.println("Set operation is not supported by this list");
        }
        System.out.println(lst);
    }

    private static void method2_ObjectArray() {
        System.out.println("Method 2");
        Integer[] arr = {1,2,3,4,5};
        AutoBoxedAsList(arr);
    }

    private static void AutoBoxedAsList(Integer[] arr) {
        // Note : Always use <Type> with collections
        List<Integer> lst = Arrays.asList(arr);
        System.out.println(lst.size());
        System.out.println(lst);
        System.out.println(lst.toString());
    }


    private static void method1_intArray() {
        System.out.println("Method 1");
        int[] arr = {11, 22, 33, 44, 55};
        primitiveAsList(arr);
    }

    private static void primitiveAsList(int[] arr) {
        // Note : Compile time error : Type argument cannot be of primitive type
      //  List <int> lst1 = Arrays.asList(arr);
        List lst = Arrays.asList(arr);
        System.out.println(lst.size());
        System.out.println(lst);
        System.out.println(lst.toString());

        System.out.println("This list has just one item, which is array of int");

        for (Object obj : lst)
        {
            System.out.println(obj);

            // Caveat : Would throw a Runtime ClassCastException is there is type mismatch
         //   int[] ar = (int[]) obj;

            // Note : just using `int[] ar` is throwing compile time error : Variable might not be initialized
            // Question : How come using `= null` not throwing this error
            int[] ar = null;

            if(obj instanceof int[])
            {
                System.out.println("The Object is an int array");
                ar = (int[]) obj;
            }
            System.out.println("Printing elements");
            System.out.println(Arrays.toString(ar));
        }
    }
}
