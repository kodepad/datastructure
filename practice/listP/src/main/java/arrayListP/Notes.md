## List

* A collection where order of insertion is maintained i.e Ordered Collection. 
* Duplicated are allowed
* null is allowed
---
## Implementation
### General Purpose
* ArrayList
* LinkedList
### Special Purpose
* Vector
* Stack
* CopyOnWriteArrayList

###Usage [2]
* Arraylist
    * Offers constant-time positional access and is just plain fast. 
    * It does not have to allocate a node object for each element in the List, and it can take advantage of System.arraycopy when it has to move multiple elements at the same time. 
    * Think of ArrayList as Vector without the synchronization overhead.
    * ArrayList has one tuning parameter — the initial capacity, which refers to the number of elements the ArrayList can hold before it has to grow. 
    
* LinkedList
    * If you frequently add elements to the beginning of the List or iterate over the List to delete elements from its interior, you should consider using LinkedList. 
    * These operations require constant-time in a LinkedList and linear-time in an ArrayList. 
    * But you pay a big price in performance. Positional access requires linear-time in a LinkedList and constant-time in an ArrayList. 
    * Furthermore, the constant factor for LinkedList is much worse. 
    * If you think you want to use a LinkedList, measure the performance of your application with both LinkedList and ArrayList before making your choice; ArrayList is usually faster.
    * LinkedList has no tuning parameters and seven optional operations
        * clone
        * addFirst
        * getFirst
        * removeFirst
        * addLast
        * getLast
        * removeLast
    * LinkedList also implements the Queue interface.
    
* CopyOnWriteArrayList [3]
    *   A thread-safe variant of ArrayList in which all mutative operations (add, set, and so on) are implemented by making a fresh copy of the underlying array.
    * This is ordinarily too costly, but may be more efficient than alternatives when traversal operations vastly outnumber mutations, and is useful when you cannot or don't want to synchronize traversals, yet need to preclude interference among concurrent threads.
    * This implementation is well suited to maintaining event-handler lists, in which change is infrequent, and traversal is frequent and potentially time-consuming.
    
* Vector
    * The Vector class implements a growable array of objects.
    * Like an array, it contains components that can be accessed using an integer index. 
    * However, the size of a Vector can grow or shrink as needed to accommodate adding and removing items after the Vector has been created.
    * The iterators returned by this class's iterator and listIterator methods are fail-fast
    * Vector is synchronized. If a thread-safe implementation is not needed, it is recommended to use ArrayList in place of Vector.
    * If you need synchronization, a Vector will be slightly faster than an ArrayList synchronized with Collections.synchronizedList. 
    * But Vector has loads of legacy operations, so be careful to always manipulate the Vector with the List interface or else you won't be able to replace the implementation at a later time.

* Arrays.asList
    * List view of an Array [4]
    * Use it if your List is fixed in size — that is, you'll never use remove, add, or any of the bulk operations other than containsAll
    * This class is roughly equivalent to Vector, except that it is unsynchronized.[5]

* Stack
    * Extends Vector, which implements List
    
    
### Generics [1]

* Without a generic type set on the List variable declaration the Java compiler only knows that the List contains Object instances. 
* Thus, we need to cast them to the concrete class (or interface) that you know the object to be of.
* It helps you avoid inserting the wrong types of objects into the List. 
* It enables you to retrieve the objects from the List without having to cast them their real type. And - it helps the reader of your code see what type of objects the List is supposed to contain. 
* You should only omit a generic type if you have very good reasons to do so.

[1] http://tutorials.jenkov.com/java-collections/list.html
[2] https://docs.oracle.com/javase/tutorial/collections/implementations/list.html
[3] https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/CopyOnWriteArrayList.html
[4] https://docs.oracle.com/javase/tutorial/collections/implementations/convenience.html
[5] https://docs.oracle.com/javase/8/docs/api/java/util/ArrayList.html