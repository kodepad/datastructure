package ValidateSubsequence;

import java.util.Arrays;
import java.util.List;

public class Subsequence {

    public static void main(String[] args) {
        List<Integer> input = Arrays.asList(5, 1, 22, 25, 6, -1, 8, 10);
        List<Integer> sequence = Arrays.asList(22, 25, 6);
        System.out.println(isValidSubsequence(input,sequence));
    }

    public static boolean isValidSubsequence(List<Integer> array, List<Integer> sequence) {
        int arrIndex = 0;
        int seqIndex = 0;

        for (int i = 0; i < array.size(); i++) {
            if (seqIndex < sequence.size() && sequence.get(seqIndex).equals(array.get(arrIndex))) {
                seqIndex += 1;
            }
            arrIndex += 1;
        }
        return seqIndex == sequence.size();
    }
}


