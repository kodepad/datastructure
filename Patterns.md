### Questions

* How to handle edge cases
    * +1 / -1 / <= / > =
### Binary Search

* Time complexity : log2N
* Works on sorted elements 



####  Questions

* Find number of rotations in a circularly sorted array
* Search an element in a circular sorted array
* Find first or last occurrence of a given number in a sorted array
* Count occurrences of a number in a sorted array with duplicates
* Find smallest missing element from a sorted array
* Find Floor and Ceil of a number in a sorted array
* Search in a nearly sorted array in O(logn) time
* Find number of 1’s in a sorted binary array
* Find the peak element in an array
* Exponential search

### Sliding Window

https://medium.com/@codingfreak/top-problems-on-sliding-window-technique-8e63f1e2b1fa

* Works on structures which can be iterated over sequentially
    * Strings
    * Arrays
    * LinkedList
    
* Question like finding
    * Minimum
    * Maximum
    * Longest
    * Shortest
    * Running Average
    * Contained

* Similarities in question
    * Everything is grouped in sequence (substring, subarray)
    * Everything specifies to a sequential criteria like longest, smallest, contains
    * Minimize / maximize something
       
* Approach
    * Look for one or two criteria points and fixate the window around that
####  Questions  
* Fixed length
    * Max sum subarray of size k
    * Smallest sum subarray
* Dynamic Variant
    * Smallest sum >= some value S

* Dynamic Variant with ADT
    * Additional hash map, hash set and arrays
    * Longest substring with no more than k distinct characters
        * As dealing with distinct characters, will need to use ADT
    * String permutation
    
### Links



https://medium.com/@codingfreak/binary-search-practice-problems-4c856cd9f26c